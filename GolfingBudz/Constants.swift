//
//  Constants.swift
//  GolfingBudz
//
//  Created by Surya on 19/04/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

class Constants {
    static let BASE_URL:String = "https://golfbudz-api.herokuapp.com/api"
    
    static let REGISTER:String = BASE_URL + "/register"
    
    static let COUNTRIES:String = BASE_URL + "/countries"
    static let LOGIN:String = BASE_URL + "/login"
    static let SOCIAL_LOGIN:String = BASE_URL + "/users/social/auth"
    static let REQUEST_STATUS = BASE_URL + "/request/status"
    
    static let LOGOUT:String = BASE_URL + "/logout"
    
    static let SELL_ITEMS:String = BASE_URL + "/items"
    static let BUY_ITEMS:String = BASE_URL + "/items"
    static let GET_EVENTS:String = BASE_URL + "/events"
    static let EVENT_LIKE:String = BASE_URL + "/events/like"
    static let EVENT_ATTEND:String = BASE_URL + "/events/attendees"
    static let CLUB_JOIN:String = BASE_URL + "/clubs/join"
    static let GET_CLUBS:String = BASE_URL + "/clubs"
    static let POST:String = BASE_URL + "/posts"
    static let POST_LIKE:String = BASE_URL + "/posts/like"
    
    static let FORGOT_PASS: String = BASE_URL + "/forgotpassword"
    static let REQUEST_FRIEND: String = BASE_URL + "/friend/request"
    static public func getPostsUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)/posts"
    }
    
    static public func getPostCommentUrl(postId: String) -> String{
        return BASE_URL + "/posts/comments/\(postId)"
    }
    
    static public func getSendPostCommentUrl() -> String{
        return BASE_URL + "/posts/comment"
    }
    
    static public func getCategories() -> String{
        return BASE_URL + "/categories"
    }
    
    static public func getRegion() -> String{
        return BASE_URL + "/region"
    }
    
    static public func getIndustry() -> String{
        return BASE_URL + "/industry"
    }
    
    static public func getProfession() -> String{
        return BASE_URL + "/profession"
    }
    
    static public func getUsers() -> String{
        return BASE_URL + "/users"
    }
    
    static public func getClubsUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)/clubs"
    }
    
    static public func getGroupsUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)/clubs"
    }
    
    static public func getGroupUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)/groups"
    }
    
    static public func getFriendsUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)/friends"
    }
    
    static public func getFriendSearchUrl(userId: Int, friendName: String) -> String{
        return BASE_URL + "/users/\(userId)/friends/\(friendName)/search"
    }
    
    static public func getJoinRequests(userId: Int) -> String{
        return BASE_URL + "/playrequests/\(userId)"
    }
    
    static public func getNotificationsUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)/notifications"
    }
    
    static public func createPlayRequest() -> String{
        return BASE_URL + "/playrequests"
    }
    
    static public func getProfileUrl(userId: Int) -> String{
        return BASE_URL + "/users/\(userId)"
    }
}
