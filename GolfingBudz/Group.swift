//
//  Group.swift
//  GolfingBudz
//
//  Created by Surya on 29/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Group: Mappable {
    var id: String?
    var title: String?
    var description: String?
    var category: String?
    var operatingHours: String?
    var userId: Int?
    var isActive: Bool?
    var dateModified:String?
    var dateCreated: String?
    var image: String?
    var video: String?
    var userName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        category <- map["category"]
        operatingHours <- map["operatingHours"]
        userId <- map["userId"]
        isActive <- map["isActive"]
        dateModified <- map["dateModified"]
        dateCreated <- map["dateCreated"]
        image <- map["image"]
        video <- map["video"]
        userName <- map["userName"]
    }
}
