//
//  EventDetailViewController.swift
//  GolfingBudz
//
//  Created by Ramandeep Singh on 12/10/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ImageSlideshow

class EventDetailViewController: UIViewController, UIScrollViewDelegate {

    var dict : NSDictionary!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDesc : UILabel!
    @IBOutlet var slideshow: ImageSlideshow!
    override func viewDidLoad() {
        
        lblTitle.text = dict.value(forKey: "title") as? String
        lblDesc.text = dict.value(forKey: "description") as? String
        lblDesc.sizeToFit()
        let urlStrings:[String] = Helper.getUrlArray(urls: (dict.value(forKey: "images") as? String)!)

        if(urlStrings[0] != "")
        {
        var kingfisherSource : [InputSource] = [KingfisherSource(urlString: urlStrings[0])!]
        
//        let kingfisherSource = [KingfisherSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]

        
        for var i in 1..<urlStrings.count {
            if(urlStrings[i] != "")
            {
            kingfisherSource.append(KingfisherSource(urlString:urlStrings[i])!)
            }
        }
        
       
        slideshow.slideshowInterval = 2.0
    
        slideshow.pageControlPosition = PageControlPosition.underScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        slideshow.pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        slideshow.setImageInputs(kingfisherSource)
        }

        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
