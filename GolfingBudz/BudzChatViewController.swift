//
//  BudzChatViewController.swift
//  GolfingBudz
//
//  Created by Ramandeep Singh on 09/10/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import FirebaseDatabase

class BudzChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var arrMsgs : NSMutableArray! = []
    var arrKeys : NSMutableArray! = []

    @IBOutlet var tblChats: UITableView!
    
    override func viewDidLoad() {
      
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        arrMsgs = []
        let userId = DataPersistor.sharedInstance.getUserId()
        
        let databaseRoot = Database.database().reference()
        let databaseRoot1 = databaseRoot.child("channel")
        let databaseChat = databaseRoot1.child(String(format:"%d",userId!))
        let query = databaseChat.queryLimited(toLast: 10)
        
        // Observe the query for changes, and if a child is added, call the snapshot closure
        _ = query.observe(.childAdded, with: { [weak self] snapshot in
            
            let userSnap = snapshot
            let  uid = userSnap.key //the uid of each user
            self?.arrKeys.add(uid)
            
            // Get all the data from the snapshot
            let data        = snapshot.value as! NSMutableDictionary
            
            self?.arrMsgs.add(data)
            self?.tblChats.reloadData()
            
        })
    }
    
    @IBAction func back(_sender : UIButton!)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMsgs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        var cellId = "cell"
        
        let item :NSMutableDictionary! = arrMsgs[index] as! NSMutableDictionary
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuTitleCell

        cell.titleText.text = item.value(forKey: "name") as? String
        cell.lblMsg.text = item.value(forKey: "message") as? String
        cell.lblTime.text = item.value(forKey: "postedOn") as? String
        cell.img.layer.cornerRadius = cell.img.frame.size.width/2
        cell.img.clipsToBounds = true
        
        tableView.separatorColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item :NSMutableDictionary! = arrMsgs[indexPath.row] as! NSMutableDictionary
      
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "ChatsViewController") as! ChatsViewController

        
        destinationVC.strReceiverId = arrKeys[indexPath.row] as! String
        destinationVC.strReceiverName = item.value(forKey: "name") as! String

        
        self.navigationController?.pushViewController(destinationVC, animated: true)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
