//
//  SearchFriendViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 28/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class SearchFriendViewCell: UITableViewCell {
    
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnProfile: UIButton!

    @IBOutlet weak var btnBlock: UIButton!
    
    @IBOutlet weak var btnMessage: UIButton!
    
    @IBOutlet weak var btnPlayRequest: UIButton!
    
    @IBOutlet weak var pic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
