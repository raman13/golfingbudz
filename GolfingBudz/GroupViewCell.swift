//
//  GroupViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 29/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class GroupViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var img: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
