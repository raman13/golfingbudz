//
//  GolfClubViewController.swift
//  GolfingBudz
//
//  Created by Surya on 28/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ObjectMapper

class EventsViewController: UITableViewController, ServiceCallback  {
    
    var data = [Event]()
    var filteredData = [Event]()
    var dictEvent : NSDictionary! = nil
    @IBOutlet var tblEvents : UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "+", style: .done , target: self, action: #selector(EventsViewController.addClicked))
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.hideKeyboardWhenTappedAround()
        
        //self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_inner.jpg"))
      //  searchController.searchResultsUpdater = self
       // searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
       // tableView.tableHeaderView = searchController.searchBar
        
        getData()
        
    }
    
    func addClicked(){
        self.performSegue(withIdentifier: "toCreateEvent", sender: self);
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        // Add a background view to the table view
        let backgroundImage = UIImage(named: "bg_inner.jpg")
        let imageView = UIImageView(frame: self.view.frame)
        imageView.image = backgroundImage
        //  self.tableView.backgroundView = imageView
        // tableView.backgroundColor = UIColor.clear
        //self.view.addSubview(imageView)
        //self.view.sendSubview(toBack: imageView)
    }
    
    func getData(){
        
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.GET_EVENTS, bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_events, showLoader: true)
    }
    
    func updateRequestStatus(post: Notification){
        
        let params = Mapper().toJSON(post)
        // let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.REQUEST_STATUS, bodyParams: params as [String : AnyObject], method: .put, token:"", type: ServiceResponse.SERVICE_TYPE.REQUEST_STATUS, showLoader: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return data.count// your number of cell here
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        let item = data[index]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventViewCell", for: indexPath) as! EventViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        //cell.backgroundColor = UIColor.clear
        
        // Configure the cell...
        
        
        if let imgs = item.image{
            
            let urlStrings:[String] = Helper.getUrlArray(urls: imgs)
            if let urlFirst:String = urlStrings[0]{
                if let url = URL.init(string: urlFirst) {
                    cell.imgItem.kf.setImage(with: url)
                    cell.imgItem.contentMode = UIViewContentMode.scaleAspectFill
                    cell.imgItem.clipsToBounds = true
                    
                }
            }
        }
        
        cell.date.text = item.updatedAt
        cell.postText?.text = item.description
        cell.name.text = item.title
        cell.likeBtn.setTitle("\(item.likeCount!) Like", for: .normal)
        let userId = DataPersistor.sharedInstance.getUserId()
        
        if(item.likes?.contains(String(format:"%d",userId!)))!
        {
            cell.likeBtn.setImage(UIImage(named:"likeFilled"), for: UIControlState.normal)
        }
        else
        {
            cell.likeBtn.setImage(UIImage(named:"like"), for: UIControlState.normal)

        }
        if(item.eventsAttendess?.contains(String(format:"%d",userId!)))!
        {
            cell.commentBtn.setImage(UIImage(named:"attendFilled"), for: UIControlState.normal)
        }
        else
        {
            cell.commentBtn.setImage(UIImage(named:"attend"), for: UIControlState.normal)
            
        }
        //cell.commentBtn.setTitle("\(item.noComments!) Comment", for: .normal)
        //cell.commentBtn.setLeftImage(imageName: "", padding: 4)
        cell.likeBtn.addTarget(self, action: #selector(EventsViewController.likeClicked(_:)), for: UIControlEvents.touchUpInside)
        cell.likeBtn.tag = indexPath.row

        cell.commentBtn.addTarget(self, action: #selector(EventsViewController.commentClicked(_:)), for: UIControlEvents.touchUpInside)
        cell.commentBtn.tag = indexPath.row
        

        
        let shareRecog = UITapGestureRecognizer(target:self, action:#selector(EventsViewController.shareClicked(_:)))
        cell.shareBtn.tag = (indexPath as NSIndexPath).row
        cell.shareBtn.isUserInteractionEnabled = true
        cell.shareBtn.addGestureRecognizer(shareRecog)
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.item
        let item = data[index]
        if(item.image == nil)
        {
            item.image = ""
        }
        dictEvent = ["title" : item.title!,
                     "description" : item.description!,
                     "images": item.image!  ]
        
        
        self.performSegue(withIdentifier: "toEventDetail", sender: self);
        

    }
    
    func likeClicked(_ btn: UIButton!){
        
        
        let index = btn.tag
        
        let item = data[index]
        
        let userId = DataPersistor.sharedInstance.getUserId()
        
        
        if(item.likes?.contains(String(format:"%d",userId!)))!
        {
            if let index = item.likes?.index(of: String(format:"%d",userId!)) {
                item.likes?.remove(at: index)
            }
            item.likeCount = item.likeCount! - 1
            let count: Int = item.likeCount!
            btn.setTitle("\(count) Like", for: .normal)
            btn.setImage(UIImage(named:"like"), for: UIControlState.normal)
        }
        else
        {
            item.likes?.append(String(format:"%d",userId!))
            item.likeCount = item.likeCount! + 1
            let count: Int = item.likeCount!
            btn.setTitle("\(count) Like", for: .normal)
            btn.setImage(UIImage(named:"likeFilled"), for: UIControlState.normal)

            
        }
        
        //print(img.view?.tag)
        //  print(img.view?.tag)
  
        let d = data[index]
        
        

        
        let params : NSDictionary! = ["userName" : DataPersistor.sharedInstance.getUserName()!,
                                      "userId" : String(format:"%d",DataPersistor.sharedInstance.getUserId()!),
                                      "eventId" : d._id!]
        
        
        
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.EVENT_LIKE, bodyParams: params as! [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.event_like, showLoader: false)
        
        
    }
    
    func commentClicked(_ btn: UIButton!){
        
        let index = btn.tag
        
        let item = data[index]
        
        let userId = DataPersistor.sharedInstance.getUserId()
        
        
        if(item.eventsAttendess?.contains(String(format:"%d",userId!)))!
        {
            if let index = item.eventsAttendess?.index(of: String(format:"%d",userId!)) {
                item.eventsAttendess?.remove(at: index)
            }
     
            btn.setTitle("Attending", for: .normal)
            btn.setImage(UIImage(named:"attend"), for: UIControlState.normal)
        }
        else
        {
            item.eventsAttendess?.append(String(format:"%d",userId!))
        
            btn.setTitle("Attending", for: .normal)
            btn.setImage(UIImage(named:"attendFilled"), for: UIControlState.normal)
            
            
        }
        
        //print(img.view?.tag)
        //  print(img.view?.tag)
        
        let d = data[index]
        
        
        
        
        let params : NSDictionary! = ["userName" : DataPersistor.sharedInstance.getUserName()!,
                                      "userId" : String(format:"%d",DataPersistor.sharedInstance.getUserId()!),
                                      "eventId" : d._id!]
        
        
        
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.EVENT_ATTEND, bodyParams: params as! [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.event_attend, showLoader: false)
    }
    
    func shareClicked(_ img: UITapGestureRecognizer){
        //print(img.view?.tag)
        //  print(img.view?.tag)
        let index = (img.view?.tag)!
        let d = data[index]
    }

    
    func connected(sender: UIButton){
        let buttonTag = sender.tag
        print(buttonTag)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //segue for the popover configuration window
        if segue.identifier == "toEventDetail" {
            if let controller = segue.destination as? EventDetailViewController {
                
                controller.dict = dictEvent
                
            }
        }
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }

    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200 {
            
            switch response.getType(){
            case ServiceResponse.SERVICE_TYPE.event_like:
                
                self.view.makeToast("You liked this Event")
                
            case ServiceResponse.SERVICE_TYPE.get_events:
                
                //print("Received")
                data.removeAll()
                if let res = Mapper<Response<Event>>().map(JSONObject: response.response){
                    data = res.data!
                }
                tableView.reloadData()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
}

