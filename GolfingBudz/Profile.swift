//
//  Profile.swift
//  GolfingBudz
//
//  Created by Surya on 21/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Profile: Mappable {
    
    var userId: Int?
    var firstName: String?
    var lastName: String?
    var userType: String?
    var email: String?
    var profileImage: String?
    var country: String?
    var handicap: String?
    var strength: String?
    var weakness: String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        userType <- map["userType"]
        email <- map["email"]
        profileImage <- map["profileImage"]
        country <- map["country"]
        handicap <- map["handicap"]
        strength <- map["strength"]
        weakness <- map["weakness"]
    }
    
}
