//
//  Helper.swift
//  GolfingBudz
//
//  Created by Surya on 21/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import UIKit

class Helper{
    
    static public func getUrlArray(urls: String) -> [String]{
        
       return urls.components(separatedBy: "|")
        
    }
    
    static public func emptyMessage(message:String, viewController:UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.bounds.size.width, height: viewController.bounds.size.height))
        messageLabel.text = message
        let bubbleColor = UIColor(red: CGFloat(255)/255, green: CGFloat(255)/255, blue: CGFloat(255)/255, alpha :1)
        
        messageLabel.textColor = bubbleColor
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "System", size: 18)
        messageLabel.sizeToFit()
        
        viewController.backgroundView = messageLabel;
        viewController.separatorStyle = .none;
    }
    
   static func getTodayString() -> String{
        
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMM yyyy : hh:mm aa"
    let result = formatter.string(from: date)
        return result
        
    }
    
    static func toUTF8(_ value: String) -> String{
        
        do{
            let encodedData = value.data(using: String.Encoding.utf8)!
            let attributedOptions = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType]
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            
            //println(attributedString.string)
            return attributedString.string
        }catch{
            return ""
        }
    }
}
