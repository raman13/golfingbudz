//
//  MenuTitleCell.swift
//  GolfingBudz
//
//  Created by Surya on 19/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class MenuTitleCell: UITableViewCell {
    
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var img: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
