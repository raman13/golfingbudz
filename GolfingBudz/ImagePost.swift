//
//  ImagePost.swift
//  GolfingBudz
//
//  Created by Surya on 19/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import UIKit

class ImagePost {
    
    enum TYPE {
        case TITLE, IMAGE, VIDEO, UPLOAD
    }
    
    var imageUI: UIImage?
    var image:String?
    var title: String?
    var type: TYPE?
    var video: String?
    var progress: Double = 0
    var isCompleted: Bool = false
    var videoURL:NSURL?
    
    init(title: String, type: TYPE) {
        self.title = title
        self.type = type
    }
    
    
}
