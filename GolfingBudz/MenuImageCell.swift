//
//  MenuImageCell.swift
//  GolfingBudz
//
//  Created by Surya on 19/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class MenuImageCell: UITableViewCell {
    
    
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var img: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
