//
//  CountryResponse.swift
//  GolfingBudz
//
//  Created by Surya on 01/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Response<T: Mappable>: ServiceResponse {
    
    var data:[T]?
    
    // MARK: Mappable
    required init?(map: Map) {
        // subClasses must call the constructor of the base class
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
    
}
