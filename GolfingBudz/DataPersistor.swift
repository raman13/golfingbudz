//
//  DataPersistor.swift
//  GolfingBudz
//
//  Created by Surya on 19/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

class DataPersistor {
    
    static let sharedInstance = DataPersistor()
    
    let IS_LOGGED_IN:String = "IS_LOGGED_IN"
    let VIRTUAL_IMEI = "VIRTUAL_IMEI"
    let USERID = "USERID"
    let USER_NAME = "USER_NAME"
    let PROFILE_URL = "PROFILE_URL"
    
    let defaults = Foundation.UserDefaults.standard
    
    func setLoggedIn(_ isLoggedIn: Bool){
        defaults.set(isLoggedIn, forKey: IS_LOGGED_IN)
    }
    
    func isLoggedIn() -> Bool{
        return defaults.bool(forKey: IS_LOGGED_IN)
    }
    
    func setVirtualIMEI(id: String){
        defaults.set(id, forKey: VIRTUAL_IMEI)
    }
    
    func getVirtualIMEI() -> String? {
        return defaults.string(forKey: VIRTUAL_IMEI)
    }
    
    func setUserId(id: Int){
        defaults.set(id, forKey: USERID)
    }
    
    func getUserId() -> Int? {
        return defaults.integer(forKey: USERID)
    }
    
    func setUserName(uName: String){
        defaults.set(uName, forKey: USER_NAME)
    }
    
    func getUserName() -> String? {
        return defaults.string(forKey: USER_NAME)
    }
    
    func setProfileUrl(uName: String){
        defaults.set(uName, forKey: PROFILE_URL)
    }
    
    func getProfileUrl() -> String? {
        return defaults.string(forKey: PROFILE_URL)
    }
}
