//
//  ServiceResponse.swift
//  GolfingBudz
//
//  Created by Surya on 21/04/2017.
//  Copyright © 2017 Surya. All rights reserved.
//
import Foundation
import ObjectMapper

class ServiceResponse : Mappable{
    
    enum SERVICE_TYPE {case login,social_login , register, logout, get_counteries, get_posts, post, post_like, forgot_pass, get_profile, post_event,
        update_profile, REQUEST_STATUS, create_play_request, get_friends, get_comments, post_comment, get_clubs, get_groups, search_friends, sell_items, buy_items, get_region, get_industry, get_profession,
    get_categories, get_events, create_events, join_requests, event_like, event_attend, club_follow, request_friend, on_course  }
    
    var status: Int?
    var error: AnyObject?
    var message: String?
   // var data: AnyObject?
    var response: AnyObject?
    var type: SERVICE_TYPE?
    
    init() {
        
    }
    
    // MARK: Mappable
    required init?(map: Map) {
        // subClasses must call the constructor of the base class
        // super.init(map)
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message  <- map["message"]
        //data <- map["data"]
        error <- map["error"]
    }
    
//    func setSuccess(_ isSuccess: Bool)
//    {
//        //self.isSuccess = isSuccess
//    }
//    
//    func getIsSuccess() -> Bool{
//        //return self.isSuccess!
//    }
    
    func setType(_ type: SERVICE_TYPE)
    {
        self.type = type
    }
    
    func getType() -> SERVICE_TYPE{
        return self.type!
    }
}

