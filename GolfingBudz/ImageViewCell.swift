//
//  ImageViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 19/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class ImageViewCell: UITableViewCell {

   
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var closeBtn: UIImageView!
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
