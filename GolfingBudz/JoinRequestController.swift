//
//  JoinRequestController.swift
//  GolfingBudz
//
//  Created by Surya on 25/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

//
//  SearchFriendViewController.swift
//  GolfingBudz
//
//  Created by Surya on 28/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

//
//  NotificationViewController.swift
//  GolfingBudz
//
//  Created by Surya on 28/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ObjectMapper

class JoinRequestController: UITableViewController, UISearchBarDelegate, ServiceCallback {
    
    var data = [PlayRequest]()
    var filteredData = [PlayRequest]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    var selected_id:Int?
    
    var searchActive : Bool = false
    
    // let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.hideKeyboardWhenTappedAround()
        
        //searchBar.showsScopeBar = true
        searchBar.delegate = self
        
        //self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_inner.jpg"))
        //searchController.searchResultsUpdater = self
        //searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        // tableView.tableHeaderView = searchController.searchBar
        
        
        
        getFriends()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        // Add a background view to the table view
        let backgroundImage = UIImage(named: "bg_inner.jpg")
        let imageView = UIImageView(frame: self.view.frame)
        imageView.image = backgroundImage
        //  self.tableView.backgroundView = imageView
        // tableView.backgroundColor = UIColor.clear
        //self.view.addSubview(imageView)
        //self.view.sendSubview(toBack: imageView)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(searchBar.text)")
       // findFriends()
    }
    
    func getFriends(){
        
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getJoinRequests(userId: userId!), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.join_requests, showLoader: true)
    }
    
    func updateRequestStatus(post: Notification){
        
        let params = Mapper().toJSON(post)
        // let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.REQUEST_STATUS, bodyParams: params as [String : AnyObject], method: .put, token:"", type: ServiceResponse.SERVICE_TYPE.REQUEST_STATUS, showLoader: true)
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count// your number of cell here
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        let item = data[index]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JoinRequestViewCell", for: indexPath) as! JoinRequestViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        //cell.backgroundColor = UIColor.clear
        
        // Configure the cell...
        // cell.pic.layer.borderWidth = 1
        cell.pic.layer.masksToBounds = false
        cell.pic.layer.borderColor = UIColor.black.cgColor
        cell.pic.layer.cornerRadius = cell.pic.frame.height/2
        cell.pic.clipsToBounds = true
        
       // cell.container.layer.cornerRadius = 2
        
        cell.lblTitle.text = item.userName!
        
        
        if let u = item.userImgUrl {
            //            if let urlP = NSURL(string: url.addingPercentEncoding(withAllowedCharacters: .  urlQueryAllowed)!){
            //                cell.pic.af_setImage(withURL: urlP as URL)
            //            }
            if let url = URL.init(string: u) {
                cell.pic.downloadedFrom(url: url)
            }
        }
        
        cell.lblDesc.text = item.requestInfo
        
        cell.btnPairUp.tag = indexPath.item
        
        cell.btnPairUp.addTarget(self, action: #selector(JoinRequestController.connected(sender:)), for: .touchUpInside)
        
        
        return cell
    }
    
    func connected(sender: UIButton){
        let buttonTag = sender.tag
        print(buttonTag)
        
     //   self.selected_id = data[buttonTag].userId
        //toProfileRead
        
       // self.performSegue(withIdentifier: "toProfileRead", sender: self)
    }
    
    
    func findFriends(){
        
        if let search = searchBar.text{
            let userId = DataPersistor.sharedInstance.getUserId()
            let callService = GenericService(view: self.view!, serviceCallback : self)
            callService?.execute(Constants.getJoinRequests(userId: userId!), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.join_requests, showLoader: true)
        }else{
            getFriends()
        }
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toProfileRead"{
            if let ctrl = segue.destination as? MyProfileViewController{
                ctrl.is_for_other = true
                ctrl.user_id = selected_id
            }
            
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //    func updateSearchResults(for searchController: UISearchController) {
    //        filterContentForSearchText(searchController.searchBar.text!)
    //    }
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200 {
            
            switch response.getType(){

                
                
            case ServiceResponse.SERVICE_TYPE.join_requests:
                
                //print("Received")
                data.removeAll()
                if let res = Mapper<Response<PlayRequest>>().map(JSONObject: response.response){
                    data = res.data!
                }
                tableView.reloadData()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
}


