//
//  Item.swift
//  GolfingBudz
//
//  Created by Surya on 28/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Item: Mappable {
    
    var userId: String?
    var price: String?
    var image: String?
    var description: String?
    var title: String?
    var userName: String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        price <- map["price"]
        image <- map["image"]
        description <- map["description"]
        title <- map["title"]
        userName <- map["userName"]
        
    }
    
}
