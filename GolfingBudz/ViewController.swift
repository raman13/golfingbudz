//
//  ViewController.swift
//  GolfingBudz
//
//  Created by Surya on 19/04/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ObjectMapper
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn
import FacebookLogin
import FacebookCore

class ViewController: UIViewController, UITextFieldDelegate, ServiceCallback, GIDSignInUIDelegate {
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var emailIdText: UITextField!
    
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var loginBtn: UIButton!

    @IBOutlet weak var forgotPass: UnderlinedLabel!
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.socialLogin1(_:)), name: NSNotification.Name(rawValue: "GoogleSignIn"), object: nil)

        
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        super.viewDidLoad()
        
        
        self.hideKeyboardWhenTappedAround()
        
        emailIdText.delegate = self
        passText.delegate = self
        
        forgotPass.text = "Forgot Password ?"
        // Do any additional setup after loading the view, typically from a nib.
        setUpUI()
        
        getPosts()
        
    }
   
//    @IBAction func loginButtonClicked() {
//        let loginManager = LoginManager()
//        loginManager.logIn([ .PublicProfile ], viewController: self) { loginResult in
//            switch loginResult {
//            case .Failed(let error):
//                print(error)
//            case .Cancelled:
//                print("User cancelled login.")
//            case .Success(let grantedPermissions, let declinedPermissions, let accessToken):
//                print("Logged in!")
//            }
//        }
//    }
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailIdText == textField
        {
            emailIdText.text = textField.text
        }else if passText == textField{
                passText.text = textField.text
        }
    }
    
    func setUpUI(){
        
        emailIdText.attributedPlaceholder = NSAttributedString(string: "Email Id",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        passText.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        emailIdText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        passText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
       // self.emailIdText.layer.cornerRadius = 4.0
       // self.emailIdText.layer.borderColor = UIColor(red:239/255.0, green:222/255.0, blue:179/255.0, alpha: 1.0).cgColor

       // self.emailIdText.layer.borderWidth = 1.0
        
       // self.passText.layer.cornerRadius = 4.0
      //  self.passText.layer.borderColor = UIColor(red:239/255.0, green:222/255.0, blue:179/255.0, alpha: 1.0).cgColor
        
      //  self.passText.layer.borderWidth = 1.0
        
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 20))
        
        let userIcon = UIImageView(image: UIImage(named: "user_icon"))
        
        emailIdText.leftView = indentView
        emailIdText.leftViewMode = .always
        
        let indentViewPass = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 20))
        passText.leftView = indentViewPass
        passText.leftViewMode = .always

        
        loginBtn.backgroundColor = .clear
        loginBtn.layer.cornerRadius = 0
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
       // emailIdText.useUnderline()
       // passText.useUnderline()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func socialLogin1(_ notification: NSNotification)
    {
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)

        let dict : NSDictionary! = notification.userInfo! as NSDictionary
//        let login = SocialLogin()
//        login.email = dict.value(forKey: "email") as? String
//        login.firstName = dict.value(forKey: "firstName") as? String
//         login.lastName = dict.value(forKey: "lastName") as? String
//        login.socialToken = dict.value(forKey: "socialToken") as? String
//        login.profileImage = dict.value(forKey: "profileImage") as? String

            
        let params : NSDictionary! = ["email" : dict.value(forKey: "email") as! String,
                                      "firstName" : dict.value(forKey: "firstName") as! String,
                                      "lastName" : dict.value(forKey: "lastName") as! String,
                                      "socialToken" : dict.value(forKey: "socialToken") as! String,
                                      "profileImage" : dict.value(forKey: "profileImage") as! String]
        
        
        //  login.deviceType = "iOS"
        // login.deviceId = self.getVirtualIMEI()
        // login.imeiNo = self.getVirtualIMEI()
        
      //  let bodyParams = Mapper().toJSON(login)
        
        vehicleService?.execute(Constants.SOCIAL_LOGIN, bodyParams: params as? [String : AnyObject], method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.social_login, showLoader: true)
        
        //loginToHomeView()
    
        
    }
    @IBAction func googlePlusButtonTouchUpInside(sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func onSiginTap(_ sender: Any) {
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
       // let bodyParams = ["":""]
        
        if (isValid()){
        
        let login = Login()
        login.email = emailIdText.text
        login.password = passText.text
      //  login.deviceType = "iOS"
       // login.deviceId = self.getVirtualIMEI()
       // login.imeiNo = self.getVirtualIMEI()
            
        let bodyParams = Mapper().toJSON(login)
        
        vehicleService?.execute(Constants.LOGIN, bodyParams: bodyParams as [String : AnyObject], method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.login, showLoader: true)
        
        //loginToHomeView()
        }
    }

    @IBAction func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn( [.publicProfile,.email] , viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                self.returnFBUserData()
            }
        }
    }
    
    func returnFBUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, first_name, last_name, email , picture" ])
        graphRequest.start(completionHandler: { (connection, result , error) -> Void in
            
            if ((error) != nil)
            {
                print("Error: \(error)")
            }
            else
            {
                let result1 : NSDictionary = result as! NSDictionary
                let uniqueId = result1.value(forKey: "id") as! NSString!
                let firstName = result1.value(forKey: "first_name") as! NSString!
                let lastName = result1.value(forKey: "last_name") as! NSString!
                let email = result1.value(forKey: "email") as! NSString!
                print(uniqueId) // This works
                print(firstName)
                print(lastName)
                print(email)
                let picture : String! = ((result1.value(forKey: "picture") as! NSDictionary).value(forKey: "data") as! NSDictionary).value(forKey: "url") as! String
                print(picture)
                let dict : NSDictionary! = ["email" : email!,
                                            "firstName" : firstName!,
                                            "lastName" : lastName!,
                                            "socialToken" : uniqueId!,
                                            "profileImage" : picture!]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoogleSignIn"), object: nil, userInfo : dict! as? [AnyHashable : Any])
                
                

            }
        })
    }
    
  
    
    
    
    
    func getPosts(){
        
        if let userId = DataPersistor.sharedInstance.getUserId(){
            
            if DataPersistor.sharedInstance.isLoggedIn() == true{
            
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getPostsUrl(userId: userId), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_posts, showLoader: true)
            }
        }
    }
    
    func isValid() -> Bool {
        
        guard let text = emailIdText.text, !text.isEmpty else {
            
            self.view.makeToast("Please provide email id")
            return false
        }
        
        if !text.isValidEmail{
            self.view.makeToast("Please provide valid email id")
            return false
        }
        
        guard let pass = passText.text, !pass.isEmpty else {
            
            self.view.makeToast("Please provide password")
            return false
        }
        
        return true
    }
    
    
    // Call it on successful login
    fileprivate func loginToHomeView()
    {
        // Setting and saving user as logged in
        DataPersistor.sharedInstance.setLoggedIn(true)
        
        //self.performSegueWithIdentifier("loginToHome", sender: self)
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        //        appDelegate.window?.rootViewController = appDelegate.centerContainer
        //        appDelegate.window?.makeKeyAndVisible()
        appDelegate.initHomeWithDrawer()
    }
    
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.get_posts:
                loginToHomeView()
                
            case ServiceResponse.SERVICE_TYPE.social_login:
                print("social_login")
                
                if let countryResponse = Mapper<Response<Profile>>().map(JSONObject: response.response){
                    for d in countryResponse.data!{
                        // print(d)
                        DataPersistor.sharedInstance.setUserName(uName: d.firstName!)
                        DataPersistor.sharedInstance.setUserId(id: d.userId!)
                        
                        guard let pImg = d.profileImage else {
                            return
                        }
                        DataPersistor.sharedInstance.setProfileUrl(uName: pImg)
                    }
                }
                
                loginToHomeView()
            case ServiceResponse.SERVICE_TYPE.login:
                
                print("Received")
                
                if let countryResponse = Mapper<Response<Profile>>().map(JSONObject: response.response){
                    for d in countryResponse.data!{
                       // print(d)
                        DataPersistor.sharedInstance.setUserName(uName: d.firstName!)
                        DataPersistor.sharedInstance.setUserId(id: d.userId!)
                        
                        guard let pImg = d.profileImage else {
                            return
                        }
                        DataPersistor.sharedInstance.setProfileUrl(uName: pImg)
                    }
                }
                
                loginToHomeView()
                // data.removeAll()
                // data = (tripResponse?.productWrappers)!
                // self.tableView.reloadData()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
    static public func getVirtualIMEI() -> String{
        
        if let imei = DataPersistor.sharedInstance.getVirtualIMEI(){
            return imei
        }else{
            let value =  GUIDString()
            DataPersistor.sharedInstance.setVirtualIMEI(id: value as String)
            return value as String
        }
        
    }
    
    static public func GUIDString() -> NSString {
        let newUniqueID = CFUUIDCreate(kCFAllocatorDefault)
        let newUniqueIDString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueID);
        let guid = newUniqueIDString as! NSString
        
        return guid.lowercased as NSString
    }


}

extension UITextField {
    
    func useUnderline() {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

extension NSRegularExpression {
    
    convenience init(pattern: String) {
        try! self.init(pattern: pattern, options: [])
    }
}

extension String {
    
    var isValidEmail: Bool {
        return isMatching(expression: NSRegularExpression(pattern: "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"))
    }
    
    //MARK: - Private
    
    private func isMatching(expression: NSRegularExpression) -> Bool {
        return expression.numberOfMatches(in: self, range: NSRange(location: 0, length: characters.count)) > 0
    }
}

