//
//  ViewController.swift
//  ChatApp
//
//  Created by Reinder de Vries on 11/06/2017.
//  Copyright © 2017 LearnAppMaking. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ChatsViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource
{
    var strReceiverId : String!
    var strReceiverName : String!
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet weak var chatTxt: UITextField!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var viewMsg: UIView!
    @IBOutlet weak var tableView: DesignableTableView!
    
    /// This array will store all the messages shown on screen
    var messages : NSMutableArray = []
    
    /// Lazy computed property for painting outgoing messages blue
   
    
    override func viewDidLoad()
    {
        
        
        KeyboardAvoiding.avoidingView = viewMsg
        tableView.estimatedRowHeight = 90.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.hideKeyboardWhenTappedAround()
        chatTxt.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationController?.navigationBar.isHidden = true
        
        updateTextField(text: chatTxt, placeHolderText: "Write a message...")
        
        
        
        super.viewDidLoad()
        lblTitle.text = strReceiverName
        
   //     let userId = DataPersistor.sharedInstance.getUserId()

        let child : String = strReceiverId

        // Prepare the Firebase query: all latest chat data limited to 10 items
        let databaseRoot = Database.database().reference()
        let databaseRoot1 = databaseRoot.child("chat")
        let databaseChat = databaseRoot1.child(child)
        let query = databaseChat.queryLimited(toLast: 10)
        // Observe the query for changes, and if a child is added, call the snapshot closure
        _ = query.observe(.childAdded, with: { [weak self] snapshot in
   
           
               let data  = snapshot.value as! NSMutableDictionary

                self?.messages.add(data)
            self?.tableView.reloadData()
            self?.scrollToBottom()
            
        })
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if chatTxt == textField
        {
            chatTxt.text = textField.text
        }
    }
    func updateTextField(text: UITextField, placeHolderText: String) {
        text.attributedPlaceholder = NSAttributedString(string: placeHolderText,
                                                        attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 32))
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 28, height: 28));
        
        indentView.backgroundColor = UIColor.clear
        
        imgView.image = UIImage(named: "avatar")
        //  indentView.addSubview(imgView)
        // text.leftView = indentView
        //text.leftViewMode = .always
        
        // text.backgroundColor = UIColor(red: 19/255, green: 19/255, blue: 19/255, alpha: 1)
        text.backgroundColor = UIColor.clear
        text.layer.borderColor = UIColor.clear.cgColor
        
        text.layer.cornerRadius = 8
        text.layer.borderWidth = 1
        // text.layer.borderColor = UIColor.clear as! CGColor
        
        text.useUnderline()
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if messages.count > 0{
            tableView.backgroundView = .none
        }else{
            Helper.emptyMessage(message: "There are no comments", viewController: tableView)
        }
        
        
        return messages.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let index = indexPath.item
        
        let dict : NSMutableDictionary! = messages[index] as! NSMutableDictionary
        let item = dict.value(forKey: "text") as! String
        
        let size = CGSize(width: view.frame.width - 105, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        
        let rectangleHeight = String(item).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        if(rectangleHeight < 40)
        {
            return 80
        }
        else
        {
            return rectangleHeight+80
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        let cellId = "commentCell"
        
        let dict : NSMutableDictionary! = messages[index] as! NSMutableDictionary
        let item = dict.value(forKey: "text") as! String
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CommentViewCell
        
    
        let size = CGSize(width: view.frame.width - 105, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        
        let rectangleHeight = String(item).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        let rectangleWidth = String(item).boundingRect(with: size, options: options, attributes: attributes, context: nil).width

        let height : CGFloat!
        if(rectangleHeight < 40)
        {
            height = 40
        }
        else
        {
            height = rectangleHeight + 40
        }
        
    
        if(dict.value(forKey: "from") as! String == strReceiverName)
        {
            cell.usrName.frame = CGRect(x: 15, y: 0, width: 200, height: 20)
            cell.comment.frame = CGRect(x: 15, y: 25, width: rectangleWidth, height: height)
                   cell.comment.textAlignment = NSTextAlignment.left
            cell.usrName.textAlignment = NSTextAlignment.left

            cell.viewBack.frame = CGRect(x: 10, y: 25, width: rectangleWidth + 20 , height: height)
            
            cell.viewBack.backgroundColor = UIColor.init(red: 0.28, green: 0.46, blue: 0.75, alpha: 1.0)

        }
        else
        {
            cell.usrName.frame = CGRect(x: self.view.frame.size.width-215, y: 0, width: 200, height: 20)

            cell.comment.frame = CGRect(x: self.view.frame.size.width-rectangleWidth-15, y: 25, width: rectangleWidth, height: height)
            cell.comment.textAlignment = NSTextAlignment.left
            cell.usrName.textAlignment = NSTextAlignment.right
            cell.viewBack.frame = CGRect(x: self.view.frame.size.width-rectangleWidth-25, y: 25, width: rectangleWidth+20, height: height)

            cell.viewBack.backgroundColor = UIColor.init(red: 0.82, green: 0.64, blue: 0, alpha: 1.0)

        }
        cell.viewBack.layer.cornerRadius = 5.0
        cell.comment.textColor = UIColor.black

        cell.usrName.text = dict.value(forKey: "postedOn") as? String
        cell.comment.text = item
        cell.comment.numberOfLines = 0
     
        
        return cell
        
    }
    @IBAction func sendMessage(_sender : UIButton!)
    {
        
        
        let userName = DataPersistor.sharedInstance.getUserName()
        let userId = DataPersistor.sharedInstance.getUserId()

        let child : String = strReceiverId
        let date : String = Helper.getTodayString()
        // Prepare the Firebase query: all latest chat data limited to 10 items
        let databaseRoot = Database.database().reference()
        let databaseRoot1 = databaseRoot.child("chat")
        let databaseChat = databaseRoot1.child(child).childByAutoId()
        let message : NSMutableDictionary! = ["from": userName, "postedOn": date, "text": chatTxt.text!, "to": strReceiverName]

        databaseChat.setValue(message)
        
       
        let child1 : String = String(format:"%d/%@",userId!,strReceiverId)
        let databaseRoot2 = databaseRoot.child("channel")
        let databaseChat1 = databaseRoot2.child(child1)
        
        let message1 : NSMutableDictionary! = ["chatWithId": String(format:"%d",userId!), "message": chatTxt.text!, "name": strReceiverName, "postedOn": date]
        
        databaseChat1.setValue(message1)
        
        var text2 : String! = strReceiverId.replacingOccurrences(of: "CH", with: "", options: NSString.CompareOptions.literal, range:nil)
          text2 = text2.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range:nil)
        text2 = text2.replacingOccurrences(of: String(format:"%d",userId!), with: "", options: NSString.CompareOptions.literal, range:nil)
        print(text2)
        
        let child2 : String = String(format:"%@/%@",text2,strReceiverId)
        let databaseRoot3 = databaseRoot.child("channel")
        let databaseChat2 = databaseRoot3.child(child2)
        
        let message2 : NSMutableDictionary! = ["chatWithId": text2, "message": chatTxt.text!, "name": userName!, "postedOn": date]
        
        databaseChat2.setValue(message2)
        
         chatTxt.text = ""
    }
    
    func scrollToBottom(){
     
            let indexPath = IndexPath(row: self.messages.count-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        
    }
    
    
    @IBAction func back(_sender : UIButton!)
    {
        self.navigationController?.popViewController(animated: true)
    }
    /**
         Show a dialog that asks the user to input a display name,
         and store that display name in the user defaults.
    */
   
}

