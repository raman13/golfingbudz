//
//  ProfileDetail.swift
//  GolfingBudz
//
//  Created by Surya on 22/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileDetail: Profile{
    
    var contact : String?
    var password : String?
    var dob : String?
    var emailVerified : Int?
    var roleId : Int?
    
    var isActive : Int?
    var dateCreated : String?
    var dateModified : String?
    var clubName : String?
    var description : String?
    
    var address : String?
    var city : String?
    var subRub : String?
    var operatingHours : String?
    var auth_token : String?
    
    var status : String?
    var otp : String?
    var sex : Int?
    var affiliated : String?
    var age : String?
    
    var profession : String?
    var roundsPerMonth : Int?
    var refer : String?
    var playWithUs : String?
    var playWithOther : String?
    
    var noOfHandicap : Int?
    var course : String?
    var location : String?
    
    var imeiNo: String?
    var deviceId: String?
    
    var deviceType:String = "iOS"
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        contact <- map["contact"]
        password <- map["password"]
        dob <- map["dob"]
        emailVerified <- map["emailVerified"]
        roleId <- map["roleId"]
        
        isActive <- map["isActive"]
        dateCreated <- map["dateCreated"]
        dateModified <- map["dateModified"]
        clubName <- map["clubName"]
        description <- map["description"]
        
        address <- map["address"]
        city <- map["city"]
        subRub <- map["subRub"]
        operatingHours <- map["operatingHours"]
        auth_token <- map["auth_token"]
        
        status <- map["status"]
        otp <- map["otp"]
        sex <- map["sex"]
        affiliated <- map["affiliated"]
        age <- map["age"]
        
        profession <- map["profession"]
        roundsPerMonth <- map["roundsPerMonth"]
        refer <- map["refer"]
        playWithUs <- map["playWithUs"]
        playWithOther <- map["playWithOther"]
        
        noOfHandicap <- map["noOfHandicap"]
        course <- map["course"]
        location <- map["location"]
        
        imeiNo <- map["imeiNo"]
        deviceId <- map["deviceId"]
        deviceType <- map["deviceType"]
        
    }
    
}
