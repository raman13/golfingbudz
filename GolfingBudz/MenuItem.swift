//
//  MenuItem.swift
//  GolfingBudz
//
//  Created by Surya on 19/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

class MenuItem {
    
    enum TYPE {
        case ICON, MY_PROFILE, HOME, NOTIFICATION, SEARCH_FRIEND, GOLF_CLUBS, PLAY_REQUEST, CREATE_GROUP,
        MY_GROUP, EVENTS, BUY_ITEMS, SELL_ITEMS, BUDZ_CHAT, DALES_GOLF_CHAT, ON_THE_COURSE, MORE, SHARE, LOGOUT
    }
    
    var image: String?
    var title: String?
    var type: TYPE?
    
    init(image: String, title: String, type: TYPE) {
        self.image = image
        self.title = title
        self.type = type
    }
}
