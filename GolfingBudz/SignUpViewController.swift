//
//  SignUpViewController.swift
//  GolfingBudz
//
//  Created by Surya on 20/04/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import Toast_Swift
import DropDown
import ObjectMapper

class SignUpViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, ServiceCallback {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var firstNameText: UITextField!
    
    @IBOutlet weak var lastNameText: UITextField!
    
    
    @IBOutlet weak var emailText: UITextField!

    @IBOutlet weak var passText: UITextField!
    
    
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var confirmPassText: UITextField!
    
    @IBOutlet weak var belongsToText: UITextField!
    
    @IBOutlet weak var signUp: UIButton!
    
    @IBOutlet weak var clubNameText: UITextField!
    

    @IBOutlet weak var descriptionText: UITextField!
    
    
    @IBOutlet weak var addressText: UITextField!
    
    @IBOutlet weak var cityText: UITextField!
    
    @IBOutlet weak var suburbText: UITextField!
    
    
    @IBOutlet weak var operatingHoursText: UITextField!
    
    
    @IBOutlet weak var contactNo: UITextField!
    
    @IBOutlet weak var clubView: UIView!
    
    @IBOutlet weak var clubViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var tnc: UISwitch!
    
    let dropDownCountry = DropDown()
    let belongsTo = DropDown()
    
    var countryData = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        firstNameText.delegate = self
        lastNameText.delegate = self
        emailText.delegate = self
        passText.delegate = self
        confirmPassText.delegate = self
        countryText.delegate = self
        belongsToText.delegate = self
        clubNameText.delegate = self
        descriptionText.delegate = self
        
        addressText.delegate = self
        cityText.delegate = self
        suburbText.delegate = self
        operatingHoursText.delegate = self
        contactNo.delegate = self
        
        //updateTextField(text: addressText, placeHolderText: "A")
        
        // Do any additional setup after loading the view.
        setupUI()
        
        getCountries()
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailText == textField
        {
            emailText.text = textField.text
        }else if firstNameText == textField
        {
            firstNameText.text = textField.text
        }else if lastNameText == textField
        {
            lastNameText.text = textField.text
        }else if emailText == textField
        {
            emailText.text = textField.text
        }else if passText == textField
        {
            passText.text = textField.text
        }else if confirmPassText == textField
        {
            confirmPassText.text = textField.text
        }else if countryText == textField
        {
            countryText.text = textField.text
        }else if belongsToText == textField
        {
            belongsToText.text = textField.text
        }else if clubNameText == textField
        {
            clubNameText.text = textField.text
        }else if descriptionText == textField
        {
            descriptionText.text = textField.text
        }else if addressText == textField
        {
            addressText.text = textField.text
        }else if cityText == textField
        {
            cityText.text = textField.text
        }else if suburbText == textField
        {
            suburbText.text = textField.text
        }else if operatingHoursText == textField
        {
            operatingHoursText.text = textField.text
        }else if contactNo == textField
        {
            contactNo.text = textField.text
        }
        
    }
    
    func setupUI(){
        
        updateTextField(text: firstNameText, placeHolderText: "First Name")
        updateTextField(text: lastNameText, placeHolderText: "Last Name")
        updateTextField(text: emailText, placeHolderText: "Email Id")
        updateTextField(text: passText, placeHolderText: "Password")
        updateTextField(text: confirmPassText, placeHolderText: "Confirm Password")
        updateTextField(text: countryText, placeHolderText: "Country")
        updateTextField(text: belongsToText, placeHolderText: "Belongs To")
        
        updateTextField(text: clubNameText, placeHolderText: "Club Name")
        updateTextField(text: descriptionText, placeHolderText: "Description")
        updateTextField(text: addressText, placeHolderText: "Address")
        updateTextField(text: cityText, placeHolderText: "City")
        updateTextField(text: suburbText, placeHolderText: "Subrub")
        updateTextField(text: operatingHoursText, placeHolderText: "Operating Hours")
        updateTextField(text: contactNo, placeHolderText: "Contact No")
        
        firstNameText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        lastNameText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        emailText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        passText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        confirmPassText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        countryText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        belongsToText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        clubNameText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        descriptionText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        addressText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        cityText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        suburbText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        operatingHoursText.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        contactNo.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
    
        signUp.backgroundColor = .clear
        signUp.layer.cornerRadius = 0
        signUp.layer.borderWidth = 1
        signUp.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        dropDownCountry.anchorView = countryText
        dropDownCountry.dataSource = countryData
        
        belongsTo.anchorView = belongsToText
        belongsTo.dataSource = ["Individual", "Club"]
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.countryText.text = item
        }
        
        belongsTo.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.belongsToText.text = item
            
            if index == 0{
                self.hideShowClubView(show: false)
            }else{
                self.hideShowClubView(show: true)
            }
        }
        
        hideShowClubView(show: false)

    }
    
    // Call it on successful login
    fileprivate func loginToHomeView()
    {
        // Setting and saving user as logged in
        //UserDefaults.sharedInstance.setLoggedIn(true)
        
        //self.performSegueWithIdentifier("loginToHome", sender: self)
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        //        appDelegate.window?.rootViewController = appDelegate.centerContainer
        //        appDelegate.window?.makeKeyAndVisible()
        appDelegate.initHomeWithDrawer()
    }
    
    func getProfileDetail() -> ProfileDetail? {
        
        if isValid() {
        
        let p = ProfileDetail()
        
            p.firstName = firstNameText.text
            p.lastName = lastNameText.text
            p.password = passText.text
            p.email = emailText.text
            p.country = countryText.text
            p.userType = belongsToText.text
            p.imeiNo = ViewController.getVirtualIMEI()
            p.deviceId = p.imeiNo
            
            
            if belongsToText.text == "Club"{
                
                p.clubName = clubNameText.text
                p.description = descriptionText.text
                p.address = addressText.text
                p.city = cityText.text
                p.subRub = suburbText.text
                p.operatingHours = operatingHoursText.text
                p.contact = contactNo.text
                
            }
            
        return p
        }else{
           return nil
        }
        
    }
    
    func isValid() -> Bool{
        
        guard let text = firstNameText.text, !text.isEmpty else {
            
            self.view.makeToast("Please provide first name")
            return false
        }

        guard let lastName = lastNameText.text, !lastName.isEmpty else {
            self.view.makeToast("Please provide last name")
            return false
        }
        
        guard let emailId = emailText.text, !emailId.isEmpty, emailId.isValidEmail else {
            self.view.makeToast("Please provide valid email")
            return false
        }
        
        guard let pass = passText.text, !pass.isEmpty else {
            self.view.makeToast("Please provide password")
            return false
        }
        
        guard let confirmPass = confirmPassText.text, !confirmPass.isEmpty else {
            self.view.makeToast("Please provide confirm password")
            return false
        }
        
        if pass != confirmPass{
            self.view.makeToast("Password didn't match")
            return false

        }
        
        guard let country = countryText.text, !country.isEmpty else {
            self.view.makeToast("Please select country")
            return false
        }
        
        guard let belongs = belongsToText.text, !belongs.isEmpty else {
            self.view.makeToast("Please select belongs to")
            return false
        }
        
        if belongs == "Club"{
            guard let cName = clubNameText.text, !cName.isEmpty else {
                self.view.makeToast("Please provide club name")
                return false
            }
            
            guard let des = descriptionText.text, !des.isEmpty else {
                self.view.makeToast("Please provide description")
                return false
            }
            
            guard let add = addressText.text, !add.isEmpty else {
                self.view.makeToast("Please provide address")
                return false
            }
            
            guard let city = cityText.text, !city.isEmpty else {
                self.view.makeToast("Please provide city")
                return false
            }
            
            guard let subrub = suburbText.text, !subrub.isEmpty else {
                self.view.makeToast("Please provide subrub")
                return false
            }
            
            guard let operationH = operatingHoursText.text, !operationH.isEmpty else {
                self.view.makeToast("Please provide operating hours")
                return false
            }
            
            guard let contact = contactNo.text, !contact.isEmpty else {
                self.view.makeToast("Please provide contact no")
                return false
            }
        }
        
        
        return true
    }
    
    func hideShowClubView(show: Bool){
        
        if show{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 980)
            clubView.isHidden = false
            clubViewHeight.constant = 301
        }else{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+30)

            clubView.isHidden = true
            clubViewHeight.constant = 1
        }
        
    }
    
    func getCountries(){
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
        vehicleService?.execute(Constants.COUNTRIES, bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_counteries, showLoader: true)
    }
    
    func updateTextField(text: UITextField, placeHolderText: String) {
        text.attributedPlaceholder = NSAttributedString(string: placeHolderText,
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 32))
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 28, height: 28));
        
        indentView.backgroundColor = UIColor.clear
        
        imgView.image = UIImage(named: "avatar")
      //  indentView.addSubview(imgView)
        
        text.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        text.leftView = indentView
        text.leftViewMode = .always
        
        text.layer.borderColor = UIColor.clear.cgColor
        
        text.layer.cornerRadius = 8
        text.layer.borderWidth = 1
       // text.layer.borderColor = UIColor.clear as! CGColor
        
       // text.useUnderline()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
       // view.addSubview(scrollView)
        
       // scrollView.contentSize = CGSize( width: 375, height: 1000 )

    }
    
    
    @IBAction func onLoginTap(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }


    
    @IBAction func onCountryClick(_ sender: Any) {
        
        //print("Clicked")
        
        dropDownCountry.show()
        
    }
    
    @IBAction func onSignUpTap(_ sender: Any) {
        
        //print("Clicked")
        
        if tnc.isOn {
            
            
            
            if let p = getProfileDetail(){
            
                let params = Mapper().toJSON(p)
                
                let service = GenericService(view: self.view!, serviceCallback : self)
            service?.execute(Constants.REGISTER, bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.register, showLoader: true)
            }
            
            
        }else{
            self.view.makeToast("Please accept terms and conditions")
        }
        
    }
    
    
    @IBAction func onBelongsToClick(_ sender: Any) {
        
        belongsTo.show()
        
    }
   
    
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.register:
                
               // print(response.response!)
                
                if let countryResponse = Mapper<Response<Profile>>().map(JSONObject: response.response){
                    for d in countryResponse.data!{
                        // print(d)
                        DataPersistor.sharedInstance.setUserName(uName: d.firstName!)
                        DataPersistor.sharedInstance.setUserId(id: d.userId!)
                    }
                }
                
                loginToHomeView()
                
            case ServiceResponse.SERVICE_TYPE.get_counteries:
                
                print("Received")
                self.countryData.removeAll()
                
                if let countryResponse = Mapper<Response<Country>>().map(JSONObject: response.response){
                    for d in countryResponse.data!{
                        countryData += [d.name!]
                    }
                }
                dropDownCountry.dataSource = countryData
                dropDownCountry.reloadAllComponents()
               // data.removeAll()
               // data = (tripResponse?.productWrappers)!
               // self.tableView.reloadData()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .top)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
