//
//  BuyItemCell.swift
//  GolfingBudz
//
//  Created by Surya on 28/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class BuyItemCell: UITableViewCell {

    @IBOutlet weak var pic: UIImageView!
   
    
    @IBOutlet weak var buyBtn: UIButton!
    @IBOutlet weak var priceTxt: UILabel!
    @IBOutlet weak var descTxt: UILabel!
    @IBOutlet weak var titleTxt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
