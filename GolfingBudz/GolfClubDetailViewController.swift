//
//  GolfClubDetailViewController.swift
//  GolfingBudz
//
//  Created by Ramandeep Singh on 13/10/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class GolfClubDetailViewController: UIViewController {

    var dict : NSDictionary! 
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblHours: UILabel!
    @IBOutlet var lblContact: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var img: UIImageView!

    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        if let url = dict.value(forKey: "profileImage")  {
            if let urlP = NSURL(string: (url as AnyObject).addingPercentEncoding(withAllowedCharacters: .  urlQueryAllowed)!){
                img.af_setImage(withURL: urlP as URL)
            }
        }
        lblName.text = String(format:"  %@",dict.value(forKey: "clubName") as! String)
        lblDesc.text = String(format:"  %@",dict.value(forKey: "description") as! String)
        lblHours.text = String(format:"  %@",dict.value(forKey: "operatingHours") as! String)
        lblContact.text = String(format:"  %@",dict.value(forKey: "contact") as! String)
        lblAddress.text = String(format:"  %@",dict.value(forKey: "address") as! String)
        

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
