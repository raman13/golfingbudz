//
//  JoinRequestViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 30/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class JoinRequestViewCell: UITableViewCell {

    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var btnPairUp: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
