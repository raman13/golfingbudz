//
//  Friend.swift
//  GolfingBudz
//
//  Created by Surya on 29/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Friend: Profile{
    
    var status:String?
    var chanelId:String?
    
    required init?(map: Map) {
        super.init()
        
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        status <- map["status"]
        chanelId <- map["chanelId"]
        userId <- map["userId"]
        //firstName <- map["firstName"]
        //lastName <- map["lastName"]
        //profileimage <- map["profileimage"]
    }
    
}
