//
//  ServiceCallback.swift
//  GolfingBudz
//
//  Created by Surya on 01/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

protocol ServiceCallback
{
    func onSuccess(_ response : ServiceResponse)
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
}

