//
//  Country.swift
//  GolfingBudz
//
//  Created by Surya on 01/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

import ObjectMapper

class Country: Mappable {
    
    var Id: Int?
    var name: String?
    
    // MARK: Mappable
    required init?(map: Map) {
        // subClasses must call the constructor of the base class
        // super.init(map)
    }
    
    func mapping(map: Map) {
        Id <- map["Id"]
        name  <- map["name"]
    }
    
}
