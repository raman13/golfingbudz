//
//  HomeViewController.swift
//  GolfingBudz
//
//  Created by Surya on 16/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import MMDrawerController
import ObjectMapper
import Alamofire
import AlamofireImage
import MediaPlayer
import AVKit
import AVFoundation
import BMPlayer
import Kingfisher
import CollieGallery


class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ServiceCallback  {

    var pictures = [CollieGalleryPicture]()
    @IBOutlet weak var leftMenuBtn: UIImageView!
    
    var streamPlayer : MPMoviePlayerController!
    
    @IBOutlet weak var postText: UITextField!
    
    @IBOutlet weak var videoBtn: UIButton!
    
    @IBOutlet weak var imgBtn: UIButton!
    

    @IBOutlet weak var postTable: UITableView!
    @IBOutlet weak var postBtn: UIButton!
    
    var data = [Feed]()
    
    var selectedId : String!
    var isImage: Bool = true
    var postId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        
        self.hideKeyboardWhenTappedAround()
        
        self.postText.delegate = self
        
        // Do any additional setup after loading the view.
       // self.navigationController?.navigationBar.tintColor = UIColor.white
        
        //leftMenuBtn.tintColor = UIColor.white
        
        //leftMenuBtn.image = defaultMenuImage()
        
        //videoBtn.setLeftImage(imageName: "video", padding: 8)
        //imgBtn.setLeftImage(imageName: "images", padding: 8)
        
        let userIcon = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 20))
        
        postText.leftView = userIcon
        postText.leftViewMode = .always
        
        postBtn.backgroundColor = .clear
        postBtn.layer.cornerRadius = 0
        postBtn.contentEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
        postBtn.layer.borderWidth = 1
        postBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        //postTable.backgroundColor = UIColor.clear
        
        postTable.dataSource = self
        postTable.delegate = self
        
        postText.attributedPlaceholder = NSAttributedString(string: "Whats on your golfing mind?",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        //postText.useUnderline()
        
        let tapCommentRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.leftMenuClicked(_:)))
        leftMenuBtn.isUserInteractionEnabled = true
        leftMenuBtn.addGestureRecognizer(tapCommentRecognizer)
        
        //getPosts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPosts()
    }
    
    func leftMenuClicked(_ img: UITapGestureRecognizer){
        showLeftDrawer()
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if postText == textField
        {
            postText.text = textField.text
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 56/255, green: 56/255, blue: 53/255, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if data.count > 0{
            postTable.backgroundView = .none
        }else{
            Helper.emptyMessage(message: "No posts available", viewController: postTable)
        }
        
        
        return data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.item
        
        if data[index].postType == "image" || data[index].postType == "video"{
            return 341
        }else{
        
            return 140
        }
     }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        var cellId = "titleCell"
        
        let item = data[index]
        
        cellId = "feedCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FeedViewCell
    //    cell.playBtn.isHidden = true

            //cell.backgroundColor = UIColor.clear
        cell.name?.text = item.name
        
//        cell.videoView.isHidden = true
//        cell.imgItem.isHidden = true

        if let url = item.userImgUrl{
            
            if let urlP = NSURL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!){
               // cell.profileImg?.af_setImage(withURL: urlP as URL)
            }
            
            if let url = URL.init(string: url) {
         //       cell.profileImg.downloadedFrom(url: url)
                cell.profileImg.kf.setImage(with: url)
            }
        }
      
        if item.postType == "image"{
            cell.playBtn.isHidden = true

            let tapImageGesture = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.imagesOpen(_:)))
            cell.imgItem.tag = (indexPath as NSIndexPath).row
            cell.imgItem.isUserInteractionEnabled = true
            cell.imgItem.addGestureRecognizer(tapImageGesture)
            
            cell.imgItem.isHidden = false
            cell.imgAreaHeight.constant = 200
            if let urlStr = item.image{
            
            //if let urlP = NSURL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!){
              //  cell.imgItem.af_setImage(withURL: urlP as URL)
            //}
                let urlStrings:[String] = Helper.getUrlArray(urls: urlStr)
                var urlFirst:String = urlStrings[0]
                if urlFirst == ""{
                    
                    if urlStrings.count > 1{
                        
                        urlFirst = urlStrings[1]
                        
                    }
                }
            
            if let url = URL.init(string: urlFirst) {
            //    cell.imgItem.downloadedFrom(url: url)
                cell.imgItem.kf.setImage(with: url)
                cell.imgItem.contentMode = UIViewContentMode.scaleAspectFill
                cell.imgItem.clipsToBounds = true
            }
            }
            
        }else if item.postType == "video"{
            
            cell.videoView.isHidden = false

            cell.imgAreaHeight.constant = 200
            let url = item.video!
            
            let urlStrings:[String] = Helper.getUrlArray(urls: url)
            
            if urlStrings.count > 0 {
                
                var urlFirst:String = urlStrings[0]
                if urlFirst == ""{
                    
                    if urlStrings.count > 1{
                        
                    urlFirst = urlStrings[1]
                        
                    }
                }
                

                cell.playBtn.isHidden = false
                cell.bringSubview(toFront: cell.playBtn)
                cell.playBtn.tag = indexPath.row
         //       cell.playBtn.center = CGPoint(x: self.view.center.x-10, y : cell.center.y+30)
                cell.playBtn.addTarget(self, action: #selector(HomeViewController.playVideo(_:)), for: UIControlEvents.touchUpInside)
                
                
                
                
                /*
                player.snp.makeConstraints { (make) in
                    make.top.equalTo(self.view).offset(20)
                    make.left.right.equalTo(self.view)
                    // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
                    make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
                }
                // Back button event
                player.backBlock = { [unowned self] in
                    let _ = self.navigationController?.popViewController(animated: true)
                } */
                
                
                
                /*
                 if cell.videoView.layer.sublayers == nil{
                
                let player = AVPlayer(url: NSURL(string: urlFirst)! as URL)
                
                let playerLayer = AVPlayerLayer(player: player)
                
                playerLayer.frame = cell.videoView.bounds
                //playerLayer.bounds = cell.imgItem.bounds
                playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
            
                
                cell.videoView.layer.addSublayer(playerLayer)
                
               // player.isClosedCaptionDisplayEnabled = true
                player.allowsExternalPlayback = true
                
                player.play()
                }
*/
            
           // let moviePlayer = MPMoviePlayerController(contentURL: NSURL(string: url)! as URL)
           // moviePlayer?.view.frame = cell.imgItem.bounds
           // moviePlayer?.controlStyle = MPMovieControlStyle.embedded
                
          //  cell.imgItem.layer.addSublayer(moviePlayer)
            //cell.imgItem.addSubview((moviePlayer?.view)!)
           // moviePlayer?.isFullscreen = true
            
           
            }
        
        
        }
        
        else{
            cell.imgItem.isHidden = true
            cell.videoView.isHidden = true
            cell.imgAreaHeight.constant = 1
            cell.playBtn.isHidden = true

        }
    
        
       // cell.profileImg.layer.borderWidth = 1
        cell.profileImg.layer.masksToBounds = false
        cell.profileImg.layer.borderColor = UIColor.black.cgColor
        cell.profileImg.layer.cornerRadius = cell.profileImg.frame.height/2
        cell.profileImg.clipsToBounds = true
        
        cell.date.text = item.time
        cell.postText?.text = item.text
        let userId = DataPersistor.sharedInstance.getUserId()

        if(item.likes?.contains(String(format:"%d",userId!)))!
        {
            cell.likeIcon.image = UIImage(named:"likeFilled")
        }
        else
        {
            cell.likeIcon.image = UIImage(named:"like")

        }
        
        cell.likeBtn.setTitle("\(item.noLikes!) Like", for: .normal)
        cell.commentBtn.setTitle("\(item.noComments!) Comment", for: .normal)
        //cell.commentBtn.setLeftImage(imageName: "", padding: 4)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.likeClicked(_:)))
        cell.likeIcon.tag = (indexPath as NSIndexPath).row
        cell.likeIcon.isUserInteractionEnabled = true
        cell.likeIcon.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.otherUserClicked(_:)))
        cell.profileImg.tag = (indexPath as NSIndexPath).row
        cell.profileImg.isUserInteractionEnabled = true
        cell.profileImg.addGestureRecognizer(tapGestureRecognizer1)
        
        
        let tapCommentRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.commentClicked(_:)))
        cell.commentBtn.tag = (indexPath as NSIndexPath).row
        cell.commentBtn.isUserInteractionEnabled = true
        cell.commentBtn.addGestureRecognizer(tapCommentRecognizer)
        
        let shareRecog = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.shareClicked(_:)))
        cell.shareBtn.tag = (indexPath as NSIndexPath).row
        cell.shareBtn.isUserInteractionEnabled = true
        cell.shareBtn.addGestureRecognizer(shareRecog)
            
        return cell
        
    }
    func playVideo(_ btn: UIButton!)
    {
        let index = (btn.tag)
        let item = data[index]
        let url = item.video!
        
        let urlStrings:[String] = Helper.getUrlArray(urls: url)
        
        if urlStrings.count > 0 {
            
            var urlFirst:String = urlStrings[0]
            if urlFirst == ""{
                
                if urlStrings.count > 1{
                    
                    urlFirst = urlStrings[1]
                    
                }
            }
            streamPlayer = MPMoviePlayerController(contentURL: NSURL(string:urlFirst)! as URL)
        streamPlayer.view.frame = self.view.bounds
        self.view.addSubview(streamPlayer.view)
            NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.moviePlayerDidFinishPlaying(notification:)) , name: NSNotification.Name.MPMoviePlayerDidExitFullscreen, object: streamPlayer)

            streamPlayer.isFullscreen = true
        // Play the movie!
        streamPlayer.play()
        }
        
    }
    func moviePlayerDidFinishPlaying(notification: NSNotification) {
        streamPlayer.pause()
        streamPlayer.view.removeFromSuperview()
        
    }
    func imagesOpen(_ img: UITapGestureRecognizer){
         pictures = [CollieGalleryPicture]()
        print("Inside share")
        
        
        let index = (img.view?.tag)!
        let item = data[index]
        
        if item.postType == "image"{
            if let urlStr = item.image{
                let urlStrings:[String] = Helper.getUrlArray(urls: urlStr)
                
                for var i in 0..<urlStrings.count {
                    
                    let url = urlStrings[i]
                    if(url != "")
                    {
                    let picture = CollieGalleryPicture(url: url)
                    pictures.append(picture)
                    }
                    
                }
                let gallery = CollieGallery(pictures: pictures)
                gallery.presentInViewController(self)
            }
        }
        
        
    }
    
    func shareClicked(_ img: UITapGestureRecognizer){
        
        print("Inside share")
        
        let index = (img.view?.tag)!
        let d = data[index]
        
        let textToShare = d.text
        
        if let myWebsite = d.image {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.postTable
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func otherUserClicked(_ img: UITapGestureRecognizer){

        let index = (img.view?.tag)!
        let tappedImage = img.view as! UIImageView
        
        let item = data[tappedImage.tag]
        
        selectedId = item.userId
        self.performSegue(withIdentifier: "toProfileRead", sender: self)
        
    }
    func likeClicked(_ img: UITapGestureRecognizer){
        //print(img.view?.tag)
        let index = (img.view?.tag)!
        let tappedImage = img.view as! UIImageView
        
          let item = data[tappedImage.tag]
        
        let userId = DataPersistor.sharedInstance.getUserId()
        let indexpath : IndexPath = IndexPath(row: index, section: 0)
          let cell = self.postTable.cellForRow(at: indexpath) as! FeedViewCell
        
        
        if(item.likes?.contains(String(format:"%d",userId!)))!
        {
            if let index = item.likes?.index(of: String(format:"%d",userId!)) {
                item.likes?.remove(at: index)
            }
            tappedImage.image = UIImage(named:"like")
            
            item.noLikes = item.noLikes! - 1
            let count: Int = item.noLikes!
            cell.likeBtn.setTitle("\(count) Like", for: .normal)

        }
        else
        {
            item.likes?.append(String(format:"%d",userId!))
            tappedImage.image = UIImage(named:"likeFilled")
            item.noLikes = item.noLikes! + 1
            let count: Int = item.noLikes!
            cell.likeBtn.setTitle("\(count) Like", for: .normal)
            
        }
//        DispatchQueue.main.async {
//            self.postTable.reloadData()
//        }
        //  print(img.view?.tag)
        
        let d = data[index]
        
        let p = Post()
        p.postId = d.postId
        p.userName = d.name
        p.userId = String(format:"%d",userId!)
       // p.userName = DataPersistor.sharedInstance.getUserName()
        
        let params = Mapper().toJSON(p)
        
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.POST_LIKE, bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.post_like, showLoader: false)

        
    }


    func commentClicked(_ img: UITapGestureRecognizer){
        //print(img.view?.tag)
        //  print(img.view?.tag)
        let index = (img.view?.tag)!
        let d = data[index]
        
        self.postId = d.postId
        
        self.performSegue(withIdentifier: "toComments", sender: self);
    }

    func blendImages(_ array: NSMutableArray) -> UIImage {
        let img = array[0] as? UIImage
        let size: CGSize = img!.size
        UIGraphicsBeginImageContext(size)
        for i in 0..<array.count {
            let uiimage = array[i] as? UIImage
            uiimage?.draw(at: CGPoint.zero, blendMode: .normal, alpha: 1.0)
        }
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }


    func showLeftDrawer(){
        print("Intention is to show left drawer")
        //sendLocalPush(type: 1)
        // self.view.makeToastAtCenter("Result \(Utility.isInBetweenAlarmTimeRange())")
        
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    
    func closeDrawers(){
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.closeDrawer(animated: false, completion: nil)
    }

    
    
    @IBAction func onPostTap(_ sender: Any) {
        
        guard let pText = postText.text, !pText.isEmpty else {
            self.view.makeToast("Please enter post text")
            return
        }
        
        let p = Post()
        p.text = pText
        p.postType = "text"
        p.image = ""
        p.video = ""
        p.userName = DataPersistor.sharedInstance.getUserName()
        p.userId = String(describing: DataPersistor.sharedInstance.getUserId()!)
        p.userImgUrl = DataPersistor.sharedInstance.getProfileUrl()

        
        let params = Mapper().toJSON(p)
        
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.POST, bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.post, showLoader: true)
        
        
    }
    
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    func getPosts(){
        
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getPostsUrl(userId: userId!), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_posts, showLoader: true)
    }
    
    func logout(){
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.LOGOUT + "/" + String(userId!), bodyParams: nil, method: .put, token:"", type: ServiceResponse.SERVICE_TYPE.logout, showLoader: true)
    }
    
    func moveToLogin(){
        
        DataPersistor.sharedInstance.setUserId(id: 0)
        DataPersistor.sharedInstance.setLoggedIn(false)
        DataPersistor.sharedInstance.setVirtualIMEI(id: "")
        DataPersistor.sharedInstance.setUserName(uName: "")
        
         self.performSegue(withIdentifier: "toLogin", sender: self)
        
//        let mainStroryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let login = mainStroryboard.instantiateViewController(withIdentifier: "LoginController")
////        self.present(login, animated: false, completion: nil)
//        self.navigationController?.pushViewController(login, animated: true)
    }
    
    
    @IBAction func onImageTap(_ sender: Any) {
        
        isImage = true
        self.performSegue(withIdentifier: "toPostUploader", sender: self)
        
    }
    

    @IBAction func onVideoTap(_ sender: Any) {
       
        isImage = false
        self.performSegue(withIdentifier: "toPostUploader", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //segue for the popover configuration window
        if segue.identifier == "toPostUploader" {
            if let controller = segue.destination as? PostMediaController {
                
                controller.isImage = isImage
            
            }
        }else if segue.identifier == "toComments" {
            if let controller = segue.destination as? ChatViewController {
                
                controller.postId = self.postId!
                
            }
        }
        else if segue.identifier == "toProfileRead"{
            if let ctrl = segue.destination as? MyProfileViewController{
                let userId = DataPersistor.sharedInstance.getUserId()

                if(Int(selectedId) == userId)
                {
                    ctrl.is_for_other = false

                }
                else
                {
                ctrl.is_for_other = true
                }
                ctrl.user_id = Int(selectedId)
            }
            
        }
        
    }
    
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.logout:
                moveToLogin()
                
            case ServiceResponse.SERVICE_TYPE.post_like:
                
                self.view.makeToast("You liked this post")
           
            case ServiceResponse.SERVICE_TYPE.post:
                getPosts()
                
            case ServiceResponse.SERVICE_TYPE.get_posts:
                
                print("Received")
                data.removeAll()
                postText.text = ""
                if let countryResponse = Mapper<Response<Post>>().map(JSONObject: response.response){
                    for d in countryResponse.data!{
                        let f = Feed()
                        f.text = d.text
                        f.name = d.userName
                        f.time = d.updatedAt
                        f.noLikes = d.likeCount
                        f.image = d.image
                        f.postId = d._id
                        f.postType = d.postType
                        f.noComments = d.commentCount
                        f.userId = d.userId
                        f.video = d.video
                        f.likes = d.likes
                        f.userImgUrl = d.userImgUrl
                        data += [f]
                    }
                }
                
                postTable.reloadData()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    
    var RFC3986UnreservedEncoded:String {
        let unreservedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~/|?:&%"
        let unreservedCharsSet: CharacterSet = CharacterSet(charactersIn: unreservedChars)
        let encodedString: String = self.addingPercentEncoding(withAllowedCharacters: unreservedCharsSet)!
        return encodedString
    }
}



extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
