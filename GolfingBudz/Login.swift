//
//  File.swift
//  GolfingBudz
//
//  Created by Surya on 19/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Login: Mappable{
    
    var email: String?
    var password: String?
    var deviceType: String?
    var imeiNo: String?
    var deviceId: String?
    
    init() {}
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
        deviceType <- map["deviceType"]
        imeiNo <- map["imeiNo"]
        deviceId <- map["deviceId"]
    }
}
