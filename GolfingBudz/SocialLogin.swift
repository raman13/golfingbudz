//
//  SocialLogin.swift
//  GolfingBudz
//
//  Created by Ramandeep Singh on 11/10/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class SocialLogin: Mappable {
    
    var email: String?
    var firstName: String?
    var lastName: String?
    var profileImage: String?
    var socialToken: String?
    var deviceType: String?
    var imeiNo: String?
    var deviceId: String?
    
    init() {}
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        firstName <- map["firstName"]
         lastName <- map["lastName"]
        profileImage <- map["profileImage"]
         socialToken <- map["socialToken"]
        deviceType <- map["deviceType"]
        imeiNo <- map["imeiNo"]
        deviceId <- map["deviceId"]
    }
}
