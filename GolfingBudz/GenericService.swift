//
//

import Foundation
import Alamofire
import ObjectMapper
import IJProgressView
import UIKit

class GenericService : ProgressViewProtocol{
    
    var view: UIView?
    var callback: ServiceCallback?
    
    init?(serviceCallback: ServiceCallback)
    {
        self.callback = serviceCallback
    }
    
    init?(view: UIView, serviceCallback : ServiceCallback)
    {
        self.view = view
        self.callback = serviceCallback
    }
    
    func showProgress()
    {
        if view != nil{
            IJProgressView.shared.showProgressView(view!)
        }
    }
    
    func hideProgress()
    {
        IJProgressView.shared.hideProgressView()
    }
    
    func execute(_ url: String, bodyParams:[String:AnyObject]?, method: HTTPMethod, token: String, type: ServiceResponse.SERVICE_TYPE, showLoader: Bool)
    {
        if (showLoader){
            showProgress()
        }
        //print("Logging In with \(self.userId) and pass \(passwd) at url \(URL)")
        
        let headers: HTTPHeaders = [
            //"security-token": "\(token)",
            "Accept": "application/json",
            "Content-Type":"application/json"
            //"Accept-encoding": "gzip"
        ]
        
        if bodyParams == nil{
        
        Alamofire.request(url, method: method, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).responseJSON{
            response in
            
            debugPrint(response)
            self.hideProgress()
            
            switch response.result {
            case .success:
                // debugPrint(response.data!)
                // debugPrint(response.result.value!)
                // print("Validation Successful")
                if let serviceResponse = Mapper<ServiceResponse>().map(JSONObject: response.result.value)
                {
                    //                    //  print("Service Result \(serviceResponse.result)")
                    //                    //  print(serviceResponse.errorCode)
                    //                    // print(serviceResponse.token)
                    if let res = serviceResponse.status{
                        if res == 200 {
                            //                            ///  print("Success")
                            //                            //serviceResponse.setSuccess(true)
                            serviceResponse.setType(type)
                            serviceResponse.response = response.result.value as AnyObject?
                            //                            //serviceResponse.result = responseObject as AnyObject?
                            self.callback?.onSuccess(serviceResponse)
                        }else{
                            //                            //  print("failure")
                            //                            //serviceResponse.setSuccess(false)
                            if serviceResponse.error != nil{
                                self.callback?.onError(serviceResponse.message!, type: type)
                            }else{
                                self.callback?.onError("Error", type: type)
                            }
                            //                        }
                        }
                    }
                    //                // print(response.result)
                }
                
            case .failure(let error):
                print(error)
            }
        }
        }else{
            Alamofire.request(url, method: method, parameters: bodyParams, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).responseJSON{
                response in
                
                debugPrint(response)
                self.hideProgress()
                
                switch response.result {
                case .success:
                    // debugPrint(response.data!)
                    // debugPrint(response.result.value!)
                    // print("Validation Successful")
                    if let serviceResponse = Mapper<ServiceResponse>().map(JSONObject: response.result.value)
                    {
                        //                    //  print("Service Result \(serviceResponse.result)")
                        //                    //  print(serviceResponse.errorCode)
                        //                    // print(serviceResponse.token)
                        if let res = serviceResponse.status{
                            if res == 200 {
                                //                            ///  print("Success")
                                //                            //serviceResponse.setSuccess(true)
                                serviceResponse.setType(type)
                                serviceResponse.response = response.result.value as AnyObject?
                                //                            //serviceResponse.result = responseObject as AnyObject?
                                self.callback?.onSuccess(serviceResponse)
                            }else{
                                //                            //  print("failure")
                                //                            //serviceResponse.setSuccess(false)
                                if serviceResponse.error != nil{
                                    self.callback?.onError(serviceResponse.message!, type: type)
                                }else{
                                    self.callback?.onError("Error", type: type)
                                }
                                //                        }
                            }
                        }else{
                            let sr = ServiceResponse()
                            sr.type = type
                            sr.status = 200
                            
                            self.callback?.onSuccess(sr)
                        }
                        //                // print(response.result)
                    }else{
                        
                        let sr = ServiceResponse()
                        sr.type = type
                        sr.status = 200
                        
                        self.callback?.onSuccess(sr)
                        
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    

    
}
