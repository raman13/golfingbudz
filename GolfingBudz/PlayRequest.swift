//
//  PlayRequest.swift
//  GolfingBudz
//
//  Created by Surya on 30/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class PlayRequest: Mappable{
    
    var Id: Int?
    var noOfHoles:String?
    var day: String?
    var golfClub: String?
    var teeOffTime: String?
    var type: String?
    var requestInfo: String?
    var redefineRequest: String?
    var handicap: String?
    var locations: String?
    var industry: String?
    var profession: String?
    var gender: String?
    var age: String?
    var dateCreated: String?
    var dateModified: String?
    var userId: String?
    var userName: String?
    var userImgUrl: String?
    var players: String?
    var NoOfHandicaps: AnyObject?
    var affiliated: AnyObject?
    var requestType: String?
    var status: String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Id <- map["Id"]
        noOfHoles <- map["noOfHoles"]
        day <- map["day"]
        golfClub <- map["golfClub"]
        teeOffTime <- map["teeOffTime"]
        type <- map["type"]
        requestInfo <- map["requestInfo"]
        redefineRequest <- map["redefineRequest"]
        handicap <- map["handicap"]
        locations <- map["locations"]
        industry <- map["industry"]
        profession <- map["profession"]
        gender <- map["gender"]
        age <- map["age"]
        dateCreated <- map["dateCreated"]
        dateModified <- map["dateModified"]
        userId <- map["userId"]
        userName <- map["userName"]
        userImgUrl <- map["userImgUrl"]
        players <- map["players"]
        NoOfHandicaps <- map["NoOfHandicaps"]
        affiliated <- map["affiliated"]
        requestType <- map["requestType"]
        status <- map["status"]
    }
    
}
