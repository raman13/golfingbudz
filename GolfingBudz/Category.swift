//
//  Category.swift
//  GolfingBudz
//
//  Created by Surya on 27/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: Mappable{
    
    var Id:Int?
    var displayName: String?
    var isActive: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Id <- map["Id"]
        displayName <- map["displayName"]
        isActive <- map["isActive"]
    }

    
}
