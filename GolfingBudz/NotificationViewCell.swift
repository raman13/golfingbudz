//
//  NitificationViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 28/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class NotificationViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var pic: UIImageView!

    @IBOutlet weak var okBtn: UIImageView!
    
    @IBOutlet weak var cancelBtn: UIImageView!
    
    @IBOutlet weak var container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
