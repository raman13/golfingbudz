//
//  MyProfileViewController.swift
//  GolfingBudz
//
//  Created by Surya on 21/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import DropDown
import ObjectMapper
import Alamofire
import Firebase

class MyProfileViewController: UIViewController, UITextFieldDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, ServiceCallback {
    
    var is_for_other:Bool = false
    var user_id:Int?
    
    let imagePicker = UIImagePickerController()
    
    var popover:UIPopoverController?=nil
    
    @IBOutlet weak var firstNameTxt: UITextField!

    @IBOutlet weak var lastNameTxt: UITextField!
    
    
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var genderTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    
    @IBOutlet weak var handiCapTxt: UITextField!
    
    @IBOutlet weak var affiliationTxt: UITextField!
    
    @IBOutlet weak var professionTxt: UITextField!
    
    
    @IBOutlet weak var howManyTxt: UITextField!
    
    @IBOutlet weak var whichYouPreferTxt: UITextField!
    
    @IBOutlet weak var wouldYouPlayTxt: UITextField!
    
    @IBOutlet weak var updateBtn: UIButton!
    
    @IBOutlet weak var profImage: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    let gDD = DropDown()
    let handiDD = DropDown()
    let affDD = DropDown()
    let profDD = DropDown()
    let preferDD = DropDown()
    let playGB = DropDown()
    
    var storageRef:StorageReference?
    
    var profilePic: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let storage = Storage.storage()
        storageRef = storage.reference()
        
        firstNameTxt.delegate = self
        lastNameTxt.delegate = self
        emailTxt.delegate = self
        genderTxt.delegate = self
        ageTxt.delegate = self
        handiCapTxt.delegate = self
        affiliationTxt.delegate = self
        professionTxt.delegate = self
        howManyTxt.delegate = self
        whichYouPreferTxt.delegate = self
        wouldYouPlayTxt.delegate = self
        
        imagePicker.delegate = self
        
        if is_for_other == true{
            firstNameTxt.isEnabled = false
            lastNameTxt.isEnabled = false
            emailTxt.isEnabled = false
            genderTxt.isEnabled = false
            ageTxt.isEnabled = false
            handiCapTxt.isEnabled = false
            affiliationTxt.isEnabled = false
            professionTxt.isEnabled = false
            howManyTxt.isEnabled = false
            whichYouPreferTxt.isEnabled = false
            wouldYouPlayTxt.isEnabled = false
            updateBtn.isHidden = true
        //    btnBack.isHidden = true
            
        }else{
            if let url = DataPersistor.sharedInstance.getProfileUrl(){
                
                if let url = URL.init(string: url) {
                    profImage.downloadedFrom(url: url)
                }
                
            }
        }
        
        updateTextField(text: firstNameTxt, placeHolderText: "First Name")
        updateTextField(text: lastNameTxt, placeHolderText: "Last Name")
        updateTextField(text: emailTxt, placeHolderText: "Email Id")
        updateTextField(text: genderTxt, placeHolderText: "Gender")
        updateTextField(text: ageTxt, placeHolderText: "Age")
        updateTextField(text: handiCapTxt, placeHolderText: "")
        updateTextField(text: affiliationTxt, placeHolderText: "")
        updateTextField(text: professionTxt, placeHolderText: "")
        updateTextField(text: howManyTxt, placeHolderText: "")
        updateTextField(text: whichYouPreferTxt, placeHolderText: "")
        updateTextField(text: wouldYouPlayTxt, placeHolderText: "")
        
        
       // profImage.layer.borderWidth = 1
        profImage.layer.masksToBounds = false
        profImage.layer.borderColor = UIColor.black.cgColor
        profImage.layer.cornerRadius = profImage.frame.height/2
        profImage.contentMode = UIViewContentMode.scaleAspectFill
        profImage.clipsToBounds = true

        
        
        
        updateBtn.backgroundColor = .clear
        updateBtn.layer.cornerRadius = 0
        updateBtn.layer.borderWidth = 1
        updateBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        gDD.anchorView = genderTxt
        gDD.dataSource = ["Male", "Female"]
        
        gDD.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.genderTxt.text = item
        }
        
        handiDD.anchorView = handiCapTxt
        handiDD.dataSource = ["No", "Yes"]
        
        handiDD.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.handiCapTxt.text = item
        }
        
        affDD.anchorView = affiliationTxt
        affDD.dataSource = ["No", "Yes"]
        
        affDD.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.affiliationTxt.text = item
        }
        
        profDD.anchorView = professionTxt
        profDD.dataSource = ["Business man", "Service man"]
        
        profDD.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.professionTxt.text = item
        }
        
        preferDD.anchorView = whichYouPreferTxt
        preferDD.dataSource = ["Walking", "Taking gold cart", "Both"]
        
        preferDD.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.whichYouPreferTxt.text = item
        }
        
        playGB.anchorView = wouldYouPlayTxt
        playGB.dataSource = ["Yes", "No"]
        playGB.direction = .any
        
        playGB.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.wouldYouPlayTxt.text = item
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(MyProfileViewController.onProfileImageClick(_:)))
        profImage.isUserInteractionEnabled = true
        profImage.addGestureRecognizer(tapGestureRecognizer)
        
        getProfile()

    }
    
    func onProfileImageClick(_ sender: Any){
        
        if is_for_other == false{
            onImageChosserClick()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onGenderTap(_ sender: Any) {
        
        if is_for_other == false{
            gDD.show()
        }
    }
    

    @IBAction func onHandicapTap(_ sender: Any) {
        if is_for_other == false{
            handiDD.show()
        }
    }
    
    
    @IBAction func onAffiliationTap(_ sender: Any) {
        if is_for_other == false{
            affDD.show()
        }
    }
    
    
    @IBAction func onProfessionBtnTap(_ sender: Any) {
        
        if is_for_other == false{
            profDD.show()
        }
        
    }
    
    @IBAction func onPreferTap(_ sender: Any) {
        
        if is_for_other == false{
            preferDD.show()
        }
    }
    
   
    @IBAction func onWouldYouPlayClick(_ sender: Any) {
        
        //print("Clicke")
        if is_for_other == false{
            playGB.show()
        }
    }
    
    @IBAction func onUpdateTap(_ sender: Any) {
        
        updateProfile()
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if emailTxt == textField
        {
            emailTxt.text = textField.text
        }else if firstNameTxt == textField
        {
            firstNameTxt.text = textField.text
        }else if lastNameTxt == textField
        {
            lastNameTxt.text = textField.text
        }else if genderTxt == textField
        {
            genderTxt.text = textField.text
        }else if ageTxt == textField
        {
            ageTxt.text = textField.text
        }else if affiliationTxt == textField
        {
            affiliationTxt.text = textField.text
        }else if professionTxt == textField
        {
            professionTxt.text = textField.text
        }else if handiCapTxt == textField
        {
            handiCapTxt.text = textField.text
        }else if howManyTxt == textField
        {
            howManyTxt.text = textField.text
        }else if whichYouPreferTxt == textField
        {
            whichYouPreferTxt.text = textField.text
        }else if wouldYouPlayTxt == textField
        {
            wouldYouPlayTxt.text = textField.text
        }
        
    }
    
    func updateTextField(text: UITextField, placeHolderText: String) {
        text.attributedPlaceholder = NSAttributedString(string: placeHolderText,
                                                        attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 4, height: 32))
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 28, height: 28));
        
        indentView.backgroundColor = UIColor.clear
        
        imgView.image = UIImage(named: "avatar")
        //  indentView.addSubview(imgView)
        
        text.backgroundColor = UIColor(red: 37/255, green:
            37/255, blue: 37/255, alpha: 0.9)
        
        text.leftView = indentView
        text.leftViewMode = .always
        
        text.layer.borderColor = UIColor.clear.cgColor
        
        text.layer.cornerRadius = 8
        text.layer.borderWidth = 1
        // text.layer.borderColor = UIColor.clear as! CGColor
        
        // text.useUnderline()
        
    }
    func getProfile(){
        
        
        var userId = DataPersistor.sharedInstance.getUserId()
        
        if is_for_other == true{
            userId = user_id
        }
        
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getProfileUrl(userId: userId!), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_profile, showLoader: true)
    }
    
    func updateProfile(){
        
        if let p = self.getProfileDetail(){
            
        let params = Mapper().toJSON(p)
//        
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getProfileUrl(userId: userId!), bodyParams: params as [String : AnyObject]?, method: .put, token:"", type: ServiceResponse.SERVICE_TYPE.update_profile, showLoader: true)
        }
    }

   
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info.debugDescription)
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let callService = GenericService(view: self.view!, serviceCallback : self)
            callService?.showProgress()
            
            profImage.image = pickedImage
            
            let data = UIImageJPEGRepresentation(pickedImage, 0.5)!
            // set upload path
            let filePath = "\(String(describing: DataPersistor.sharedInstance.getUserId()!))/\("profilePhoto")"
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            self.storageRef?.child(filePath).putData(data, metadata: metaData){(metaData,error) in
                if let error = error {
                    print(error.localizedDescription)
                    callService?.hideProgress()
                    return
                }else{
                    //store downloadURL
                    let downloadURL = metaData!.downloadURL()!.absoluteString
                    print(downloadURL)
                    self.profilePic = downloadURL
                    DataPersistor.sharedInstance.setProfileUrl(uName: downloadURL)
                    
                    callService?.hideProgress()
                }
                
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func getProfileDetail() -> ProfileDetail? {
        
        if isValid() {
            
            let p = ProfileDetail()
            
            p.firstName = firstNameTxt.text
            p.lastName = lastNameTxt.text
            p.email = emailTxt.text
            p.sex = genderTxt.text == "Male" ? 1 : 0
            
            p.profession = professionTxt.text
            p.affiliated = affiliationTxt.text
            p.handicap = handiCapTxt.text
            p.age = ageTxt.text
            p.roundsPerMonth = Int(howManyTxt.text!)
            p.playWithUs = wouldYouPlayTxt.text
            p.refer = whichYouPreferTxt.text
            
            if profilePic != nil{
                p.profileImage = profilePic
            }
            
            return p
        }else{
            return nil
        }
        
    }
    
    func onImageChosserClick(){
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        //?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: alert)
            popover!.present(from: profImage.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.showsCameraControls = true
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: imagePicker)
            popover!.present(from: profImage.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
    }
    
    
    func isValid() -> Bool{
        
        guard let text = firstNameTxt.text, !text.isEmpty else {
            
            self.view.makeToast("Please provide first name")
            return false
        }
        
        guard let lastName = lastNameTxt.text, !lastName.isEmpty else {
            self.view.makeToast("Please provide last name")
            return false
        }
        
//        let emailId = emailTxt.text as! String
//        guard  (emailId.isEmpty), (emailId.isValidEmail) else {
//            self.view.makeToast("Please provide valid email")
//            return false
//        }
        
        guard let age = ageTxt.text, !age.isEmpty else {
            self.view.makeToast("Please provide age")
            return false
        }
        
        guard let hc = handiCapTxt.text, !hc.isEmpty else {
            self.view.makeToast("Please provide handicap info")
            return false
        }
        
        guard let aff = affiliationTxt.text, !aff.isEmpty else {
            self.view.makeToast("Please provide affilication info")
            return false
        }
        
        guard let prof = professionTxt.text, !prof.isEmpty else {
            self.view.makeToast("Please provide profession info")
            return false
        }
        
        guard let noRounds = howManyTxt.text, !noRounds.isEmpty else {
            self.view.makeToast("Please provide round info")
            return false
        }
        
        guard let whichYouPr = whichYouPreferTxt.text, !whichYouPr.isEmpty else {
            self.view.makeToast("Please provide which you prefer")
            return false
        }
        
        
        
        return true
    }
    
    func showSuccessAlert(){
        
        let refreshAlert = UIAlertController(title: "Success", message: "Your profile updated successfully", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: false)
        }))
        
        present(refreshAlert, animated: false, completion: nil)
    }
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.update_profile:
                
                showSuccessAlert()
                
                
            case ServiceResponse.SERVICE_TYPE.get_profile:
                
            
                if let countryResponse = Mapper<Response<ProfileDetail>>().map(JSONObject: response.response){
                    for d in countryResponse.data!{
                        
                        firstNameTxt.text = d.firstName
                        lastNameTxt.text = d.lastName
                        genderTxt.text = d.sex == 1 ? "Female" : "Male"
                        ageTxt.text = d.age
                        handiCapTxt.text = d.handicap
                        affiliationTxt.text = d.affiliated
                        professionTxt.text = d.profession
                        whichYouPreferTxt.text = d.refer
                        wouldYouPlayTxt.text = d.playWithUs
                        howManyTxt.text = String(d.roundsPerMonth!)
                        emailTxt.text = d.email
                        
                        //profImage.image = decodedimage
                        
                        if let strUrl = d.profileImage{
                            if let url = URL(string: strUrl){
                                profImage.downloadedFrom(url: url)
                            }
                        }
                        
                    }
                }
                
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
}

extension Data {
    
    /// Return hexadecimal string representation of NSData bytes
    public var hexadecimalString: NSString {
        var bytes = [UInt8](repeating: 0, count: count)
        copyBytes(to: &bytes, count: count)
        
        let hexString = NSMutableString()
        for byte in bytes {
            hexString.appendFormat("%02x", UInt(byte))
        }
        
        return NSString(string: hexString)
    }
}
