//
//  LeftDrawerTableViewController.swift
//  GolfingBudz
//
//  Created by Surya on 16/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class LeftDrawerTableViewController: UITableViewController {
    
    var data = [MenuItem]()
    
    var parentCenterController: HomeViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        let imageView = UIImageView(image: UIImage(named: "bg_innner"))
        imageView.frame = self.tableView.frame
        self.tableView.backgroundView = imageView
        self.tableView.layer.shadowRadius = 0
        self.tableView.layer.shadowOpacity = 0
    
        data += [MenuItem(image: "profile",title: "Surya", type: MenuItem.TYPE.ICON)]
        data += [MenuItem(image: "homeIcon",title: "Home",type: MenuItem.TYPE.HOME)]
        data += [MenuItem(image: "user_icon",title: "My Profile", type: MenuItem.TYPE.MY_PROFILE)]
        data += [MenuItem(image: "ring",title: "Notification", type: MenuItem.TYPE.NOTIFICATION)]
        data += [MenuItem(image: "friendIcon",title: "Search Friends", type: MenuItem.TYPE.SEARCH_FRIEND)]
        data += [MenuItem(image: "courseIcon",title: "Golf Clubs", type: MenuItem.TYPE.GOLF_CLUBS)]
        data += [MenuItem(image: "playRequestIcon",title: "Play Requests", type: MenuItem.TYPE.PLAY_REQUEST)]
        data += [MenuItem(image: "group",title: "Create Group", type: MenuItem.TYPE.CREATE_GROUP)]
        data += [MenuItem(image: "friendIcon",title: "My Group", type: MenuItem.TYPE.MY_GROUP)]
        data += [MenuItem(image: "eventIcon",title: "Events", type: MenuItem.TYPE.EVENTS)]
        data += [MenuItem(image: "cartIcon",title: "Buy Items", type: MenuItem.TYPE.BUY_ITEMS)]
 //       data += [MenuItem(image: "cartIcon",title: "Sell Items", type: MenuItem.TYPE.SELL_ITEMS)]
        data += [MenuItem(image: "chat",title: "Budz Chat", type: MenuItem.TYPE.BUDZ_CHAT)]
        data += [MenuItem(image: "chatIcon",title: "Dales Golf Chat", type: MenuItem.TYPE.DALES_GOLF_CHAT)]
        data += [MenuItem(image: "notificationIcon",title: "On the Course", type: MenuItem.TYPE.ON_THE_COURSE)]
        data += [MenuItem(image: "moreIcon",title: "More", type: MenuItem.TYPE.MORE)]
        data += [MenuItem(image: "share",title: "Share", type: MenuItem.TYPE.SHARE)]
        data += [MenuItem(image: "logout",title: "Logout", type: MenuItem.TYPE.LOGOUT)]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         let index = indexPath.item
        if index == 0 {
            return 120
        }else{
            return 45
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let index = indexPath.item
        
        var cellId = "titleCell"
        
        let item = data[index]
        
        if index == 0{
            cellId = "imageViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuImageCell
            
            cell.backgroundColor = UIColor.clear
            cell.lblText?.text = DataPersistor.sharedInstance.getUserName()
            
            let img = UIImage(named: item.image!)
            //img?.resize cell.img.frame
            cell.img?.image = img
           // cell.img?.clipsToBounds = true
            
            if let url = DataPersistor.sharedInstance.getProfileUrl(){
                
                if let url = URL.init(string: url) {
                    cell.img.downloadedFrom(url: url)
                }
                
            }
            
           // cell.img.layer.borderWidth = 1
            cell.img.layer.masksToBounds = false
            cell.img.layer.borderColor = UIColor.black.cgColor
            cell.img.layer.cornerRadius = cell.img.frame.height/2
            cell.img.clipsToBounds = true
            
            return cell
        
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuTitleCell
            
            cell.backgroundColor = UIColor.clear
            cell.imageView?.image = UIImage(named: item.image!)
            
            cell.imageView?.image = cell.imageView?.image?.imageResize(sizeChange: CGSize(width: 24, height: 24))
            
            cell.textLabel?.text = item.title
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.white
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let data = self.data[(indexPath as NSIndexPath).row]
        
        switch data.type!{
            
        case MenuItem.TYPE.HOME:
            parentCenterController?.closeDrawers()
            parentCenterController?.getPosts()
            
        case MenuItem.TYPE.LOGOUT:
            parentCenterController?.closeDrawers()
            parentCenterController?.logout()
            //toGolfClubs
        case MenuItem.TYPE.SHARE:
            
            let textToShare = "I would like to recommend you this application"
            
            if let myWebsite = NSURL(string: "https://golfbudz-api.herokuapp.com/api/") {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                activityVC.popoverPresentationController?.sourceView = tableView
                self.present(activityVC, animated: true, completion: nil)
            }
            
        case MenuItem.TYPE.EVENTS:
            
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toEvents", sender: self)
            
        case MenuItem.TYPE.BUY_ITEMS:
            
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toBuyItems", sender: self)
        
//        case MenuItem.TYPE.SELL_ITEMS:
//           
//            parentCenterController?.closeDrawers()
//            parentCenterController?.performSegue(withIdentifier: "toSellItems", sender: self)
//        
            
        case MenuItem.TYPE.GOLF_CLUBS:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toGolfClubs", sender: self)
            
        case MenuItem.TYPE.CREATE_GROUP:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toCreateGroup", sender: self)
            
        case MenuItem.TYPE.MY_GROUP:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toGroups", sender: self)
            
        case MenuItem.TYPE.PLAY_REQUEST:
            print("")
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toPlayRequestController", sender: self)
            
        case MenuItem.TYPE.MY_PROFILE:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toMyProfile", sender: self)
        
        case MenuItem.TYPE.SEARCH_FRIEND:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toSearchFriends", sender: self)
            
        case MenuItem.TYPE.NOTIFICATION:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toNotifications", sender: self)
            
        case MenuItem.TYPE.BUDZ_CHAT:
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toBudzChat", sender: self)
            
        case MenuItem.TYPE.ON_THE_COURSE:
            UserDefaults.standard.set("onCourse", forKey: "blogtype")
            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toOnCourse", sender: self)
            
        case MenuItem.TYPE.DALES_GOLF_CHAT:
            UserDefaults.standard.set("DalesChat", forKey: "blogtype")

            parentCenterController?.closeDrawers()
            parentCenterController?.performSegue(withIdentifier: "toDalesChat", sender: self)
        default:
            print("Nothing Selected")
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    func addBackground(imageName:String) {
        
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = UIImage(named: imageName)
        imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
    }
}

extension UIImage {
    
    func imageResize (sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
}

