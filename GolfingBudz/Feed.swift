//
//  Feed.swift
//  GolfingBudz
//
//  Created by Surya on 21/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

class Feed{
    
    enum FeedType {
        case Text, Image, Video
    }
    
    var postId: String?
    var name: String?
    var time: String?
    
    var image: String?
    var type: FeedType?
    
    var text: String?
    
    var video: String?
    var likes : [String]?
    var noLikes: Int?
    var noComments: Int?
    var userName: String?
    var userId: String?
    var userImgUrl: String?
    var postType: String?
    
    init() {}
}
