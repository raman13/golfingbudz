//
//  ForgotPassViewController.swift
//  GolfingBudz
//
//  Created by Surya on 06/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotPassViewController: UIViewController, UITextFieldDelegate, ServiceCallback {
    
    
    @IBOutlet weak var emailIdText: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.emailIdText.delegate = self
        // Do any additional setup after loading the view.
        emailIdText.attributedPlaceholder = NSAttributedString(string: "Email Id",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        submitBtn.backgroundColor = .clear
        submitBtn.layer.cornerRadius = 0
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        
        emailIdText.useUnderline()
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailIdText == textField
        {
            emailIdText.text = textField.text
        }
    }

    @IBAction func onSubmitTap(_ sender: Any) {
        
        guard let emailId = emailIdText.text, !emailId.isEmpty, emailId.isValidEmail else {
            self.view.makeToast("Please provide email Id")
            return
        }
        
        let p = Profile()
        p.email = emailId
        
        let params = Mapper().toJSON(p)
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
        vehicleService?.execute(Constants.FORGOT_PASS, bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.forgot_pass, showLoader: true)
        
    }
    
    func showSuccessAlert(msg: String){
        
        let refreshAlert = UIAlertController(title: "Success", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        
        present(refreshAlert, animated: false, completion: nil)
    }
   
    @IBAction func onLoginTap(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.forgot_pass:
                
                showSuccessAlert(msg: response.message!)
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
