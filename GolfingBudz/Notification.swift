import Foundation
import ObjectMapper

class Notification: Mappable {
    
    var _id: String?
    var requestId: String?
    var createdAt: String?
    var updatedAt: String?
    var status: String?
    var id: String?
    var type: String?
    var text: String?
    var title: String?
    var userImgUrl: String?
    var friendId: String?
    var userId: String?
    var userName: String?
    var notificationId: String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        createdAt <- map["createdAt"]
        updatedAt <- map["updatedAt"]
        type <- map["type"]
        text <- map["text"]
        id <- map["id"]
        title <- map["title"]
        friendId <- map["friendId"]
        userImgUrl <- map["userImgUrl"]
        userId <- map["userId"]
        userName <- map["userName"]
        status <- map["status"]
        requestId <- map["requestId"]
        notificationId <- map["notificationId"]
    }
    
}
