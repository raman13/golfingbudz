//
//  Post.swift
//  GolfingBudz
//
//  Created by Surya on 21/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Post: Mappable {
    
    var _id: String?
    var eventId : String?
    var postId: String?
    var createdAt: String?
    var updatedAt: String?
    var postType: String?
    var video: String?
    var image: String?
    var text: String?
    var shortText: String?
    var userImgUrl: String?
    var userId: String?
    var userName: String?
    var likes: [String]?
    var commentCount: Int?
    var likeCount: Int?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        postId <- map["postId"]
        createdAt <- map["createdAt"]
        updatedAt <- map["updatedAt"]
        postType <- map["postType"]
        video <- map["video"]
        image <- map["image"]
        text <- map["text"]
        shortText <- map["shortText"]
        userImgUrl <- map["userImgUrl"]
        userId <- map["userId"]
        userName <- map["userName"]
        likes <- map["likes"]
        commentCount <- map["commentCount"]
        likeCount <- map["likeCount"]
    }
    
}
