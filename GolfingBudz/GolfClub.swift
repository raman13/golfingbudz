//
//  GolfClub.swift
//  GolfingBudz
//
//  Created by Surya on 29/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class GolfClub: Mappable{
    
    var userid:Int?
    var clubName: String?
    var description: String?
    var profileImage: String?
    var country: String?
    var contact: String?
    var operatingHours: String?
    var address: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userid <- map["userId"]
        clubName <- map["clubName"]
        description <- map["description"]
        profileImage <- map["profileImage"]
        country <- map["country"]
        contact <- map["contact"]
        operatingHours <- map["operatingHours"]
        address <- map["address"]
    }
    
    
    
}

