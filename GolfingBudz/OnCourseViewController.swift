//
//  OnCourseViewController.swift
//  GolfingBudz
//
//  Created by Ramandeep Singh on 17/10/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ObjectMapper

class OnCourseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ServiceCallback {
    @IBOutlet weak var tableView: DesignableTableView!
    var strType : String!
    var data = [Comment]()
    @IBOutlet var lblHeader : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(UserDefaults.standard.value(forKey: "blogtype") as! String == "onCourse")
        {
            getOnCourse()
            lblHeader.text = "On the Course"
        }
        else
        {
            getDalesChat()
            lblHeader.text = "Dales Golf Chat"


        }
        // Do any additional setup after loading the view.
    }
    
    
    func getOnCourse(){
        
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute("https://www.adcoretechnologies.com/golf-admin/api/getBlog?blogtype=1", bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.on_course, showLoader: true)
    }
    func getDalesChat(){
        
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute("https://www.adcoretechnologies.com/golf-admin/api/getBlog?blogtype=2", bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.on_course, showLoader: true)
    }
    @IBAction func back(_sender : UIButton!)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if data.count > 0{
            tableView.backgroundView = .none
        }else{
            Helper.emptyMessage(message: "There are no blogs", viewController: tableView)
        }
        
        
        return data.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let index = indexPath.item
        
        let item = data[index]
        
        let size = CGSize(width: view.frame.width - 40, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        
        let rectangleHeight = String(item.text!).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        if(rectangleHeight < 40)
        {
            return 90
        }
        else
        {
            return rectangleHeight+50
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        let cellId = "commentCell"
        
        let item = data[index]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CommentViewCell

        cell.comment.text = item.text!
        cell.comment.numberOfLines = 0
        cell.usrName.text = item.title!
        
        return cell
        
    }
    
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.on_course:
                //moveToLogin()
                data.removeAll()
                if let res = Mapper<Response<Comment>>().map(JSONObject: response.response){
                    data = res.data!
                }
                tableView.reloadData()
                
          
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
