//
//  ProgressViewProtocol.swift
//  GolfingBudz
//
//  Created by Surya on 01/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation

protocol ProgressViewProtocol{
    func showProgress()
    func hideProgress()
}
