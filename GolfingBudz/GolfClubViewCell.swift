//
//  GolfClubViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 28/06/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class GolfClubViewCell: UITableViewCell {
    
    
    @IBOutlet weak var pic: UIImageView!
    
    @IBOutlet weak var titleTxt: UILabel!
    
    @IBOutlet weak var descTxt: UILabel!
    
    @IBOutlet weak var btnFollow: UIButton!
    
    
    @IBOutlet weak var container: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
