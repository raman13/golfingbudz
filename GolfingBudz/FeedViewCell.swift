//
//  FeedViewCell.swift
//  GolfingBudz
//
//  Created by Surya on 21/05/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class FeedViewCell: UITableViewCell {

    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var postText: UILabel!

    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var likeIcon: UIImageView!

    @IBOutlet weak var imgArea: UIView!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!

    @IBOutlet weak var imgAreaHeight: NSLayoutConstraint!
    @IBOutlet weak var imgHeightConst: NSLayoutConstraint!

    @IBOutlet weak var videoHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var videoView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
