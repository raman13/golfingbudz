//
//  PostMediaController.swift
//  GolfingBudz
//
//  Created by Surya on 18/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import Firebase
import ObjectMapper
import MobileCoreServices
import MediaPlayer
import AVKit
import AVFoundation

class CreateGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, ServiceCallback{
    
    @IBOutlet weak var postText: UITextField!
    
    @IBOutlet weak var videoTableView: UITableView!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var descTxt: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var imageContainer: UIView!
    var data = [ImagePost]()
    var data1 = [ImagePost]()
    
    var isImage: Bool = true
    
    let imagePicker = UIImagePickerController()
    
    let videoPicker = UIImagePickerController()
    
    var popover:UIPopoverController?=nil
    
    var storageRef:StorageReference?
    
    var currentIndex: Int = 0
    
    var uploadTask: StorageUploadTask?
    
    var mediaType: String = "Image"
    
    @IBOutlet weak var videoContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        let storage = Storage.storage()
        storageRef = storage.reference()
        
        
        let userIcon = UIImageView(image: UIImage(named: "golf"))
        
       // postText.leftView = userIcon
        postText.leftViewMode = .always
        
        postText.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        
        videoTableView.delegate = self
        videoTableView.dataSource = self
        
        imagePicker.delegate = self
        videoPicker.delegate = self
        
        imageContainer.isHidden = true
        videoContainer.isHidden = true
        
        submitBtn.backgroundColor = .clear
        submitBtn.layer.cornerRadius = 0
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        
        if isImage == false{
            mediaType = "Video"
            imagePicker.mediaTypes =  [kUTTypeMovie as String]
        }
        
        videoPicker.mediaTypes =  [kUTTypeMovie as String]
        
        let t = "Say Something about this \(mediaType)"
        
        postText.attributedPlaceholder = NSAttributedString(string: "Name",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        descTxt.attributedPlaceholder = NSAttributedString(string: "Description",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        // Do any additional setup after loading the view.
        
        //data += [ImagePost(title: "Upload Photo", type: ImagePost.TYPE.TITLE)]
        //data += [ImagePost(title: "",type: ImagePost.TYPE.UPLOAD)]
        
        //data1 += [ImagePost(title: "Upload Video", type: ImagePost.TYPE.TITLE)]
        //data1 += [ImagePost(title: "",type: ImagePost.TYPE.UPLOAD)]
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        //  self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        // self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 56/255, green: 56/255, blue: 53/255, alpha: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSubmitTap(_ sender: Any) {
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if postText == textField
        {
            postText.text = textField.text
        }else if descTxt == textField{
            descTxt.text = textField.text
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var count = 0
        
        if self.tableView == tableView{
        
            count = data.count
        if data.count > 0{
            tableView.backgroundView = .none
        }else{
            Helper.emptyMessage(message: "Please Upload an image", viewController: tableView)
        }
        }else if self.videoTableView == tableView{
            
            count = data1.count
            if data1.count > 0{
                videoTableView.backgroundView = .none
            }else{
                Helper.emptyMessage(message: "Please Upload a video", viewController: tableView)
            }
        }
        
        
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.item
        
        if self.tableView == tableView{
        
        if data[index].type == ImagePost.TYPE.TITLE{
            return 30
        }else if data[index].type == ImagePost.TYPE.IMAGE || data[index].type == ImagePost.TYPE.VIDEO {
            return 200
        }else{
            return 140
        }
        }else if self.videoTableView == tableView{
            
            if data1[index].type == ImagePost.TYPE.TITLE{
                return 30
            }else if data1[index].type == ImagePost.TYPE.IMAGE || data1[index].type == ImagePost.TYPE.VIDEO {
                return 200
            }else{
                return 140
            }
        }else{
            return 140
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        var cellId = "MediaUploadCell"
        
        var itm:ImagePost?
        
        var isVideo = false
        
        if self.videoTableView == tableView{
            itm = data1[index]
            isVideo = true
        }else{
            itm = data[index]
        }
        
        if let item = itm {
            
        if item.type == ImagePost.TYPE.IMAGE || item.type == ImagePost.TYPE.VIDEO {
            
            cellId = "PostImageCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ImageViewCell
            
            //cell.backgroundColor = UIColor.clear
            if item.type == ImagePost.TYPE.IMAGE{
                cell.mainImage.image = item.imageUI
            }else{
                
                if cell.mainImage.layer.sublayers == nil{
                    
                    let player = AVPlayer(url: item.videoURL! as URL)
                    
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = cell.mainImage.bounds
                    playerLayer.bounds = cell.mainImage.bounds
                    
                    cell.mainImage.layer.addSublayer(playerLayer)
                    
                    
                    player.play()
                }
                
            }
            
            var tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(CreateGroupViewController.closeClicked(_:)))
            if isVideo == true {
                tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(CreateGroupViewController.closeVideoClicked(_:)))
            }
            
            
            cell.closeBtn.tag = (indexPath as NSIndexPath).row
            cell.closeBtn.isUserInteractionEnabled = true
            cell.closeBtn.addGestureRecognizer(tapGestureRecognizer)
            
            DispatchQueue.main.async {
                cell.progressBar.setProgress(Float(item.progress), animated: true)
                //                if (!item.isCompleted && item.type == ImagePost.TYPE.MEDIA){
                //                    tableView.reloadData()
                //                }
                // tableView.reloadInputViews()
                // cell.progressBar.reloadInputViews()
            }
            
            return cell
        }else if item.type == ImagePost.TYPE.TITLE{
            
            cellId = "TitleCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TitleCell
            
            //cell.backgroundColor = UIColor.clear
            //cell.textLabel?.text = item.title
            
            return cell
        }else{
            cellId = "MediaUploadCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MediaUploadCell
            
            var tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(CreateGroupViewController.uploadClicked(_:)))
            if isVideo == true {
                tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(CreateGroupViewController.uploadVideoClicked(_:)))
            }
            
            cell.uploadBtn.tag = (indexPath as NSIndexPath).row
            cell.uploadBtn.isUserInteractionEnabled = true
            cell.uploadBtn.addGestureRecognizer(tapGestureRecognizer)
            
            return cell
        }
        }else{
            
            let c = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ImageViewCell
            return c
        }
        
    }
    
    func playVideo(_ sender: UITapGestureRecognizer){
        
        let index = (sender.view?.tag)!
        
        
    }
    
    func closeClicked(_ sender: UITapGestureRecognizer){
        
        let index = (sender.view?.tag)!
        //let d = data[index]
        
        uploadTask?.cancel()
        
        data.remove(at: index)
        tableView.reloadData()
        tableView.reloadInputViews()
        
    }
        
    func closeVideoClicked(_ sender: UITapGestureRecognizer){
            
            let index = (sender.view?.tag)!
            //let d = data[index]
            
            uploadTask?.cancel()
            
            data1.remove(at: index)
            videoTableView.reloadData()
            videoTableView.reloadInputViews()
            
        }
    
    func uploadClicked(_ sender: UITapGestureRecognizer){
        
        let index = (sender.view?.tag)!
        //let d = data[index]
        
        currentIndex = index
        
        if (isFinished()) {
            onImageChosserClick(sender: sender, isVideo: false)
        }else{
            self.view.makeToast("Please wait for image to finish upload")
        }
    
    }
    
    func uploadVideoClicked(_ sender: UITapGestureRecognizer){
            
            let index = (sender.view?.tag)!
            //let d = data[index]
            
           // currentIndex = index
            
            if (isFinishedVideo()) {
                onImageChosserClick(sender: sender, isVideo: true)
            }else{
                self.view.makeToast("Please wait for video to finish upload")
            }
    }
    
    func isFinished() -> Bool{
        for d in data{
            if !d.isCompleted && (d.type == ImagePost.TYPE.IMAGE || d.type == ImagePost.TYPE.VIDEO) {
                return false
            }
        }
        return true
    }
        
        func isFinishedVideo() -> Bool{
            for d in data1{
                if !d.isCompleted && (d.type == ImagePost.TYPE.IMAGE || d.type == ImagePost.TYPE.VIDEO) {
                    return false
                }
            }
            return true
        }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info.debugDescription)
        
        if picker == imagePicker {
            isImage = true
        }else{
            isImage = false
        }
        
        
        if isImage{
            
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                let post = ImagePost(title: "", type: ImagePost.TYPE.IMAGE)
                
                post.imageUI = pickedImage
                
                let imgData = UIImageJPEGRepresentation(pickedImage, 0.8)!
                // set upload path
                let filePath = "\(String(describing: DataPersistor.sharedInstance.getUserId()!))/\("post")/\(String(describing: DataPersistor.sharedInstance.getUserId()!))\(Date().ticks)img.jpg"
                let metaData = StorageMetadata()
                
                metaData.contentType = "image/jpg"
                uploadTask = self.storageRef?.child(filePath).putData(imgData, metadata: metaData){(metaData,error) in
                    
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }else{
                        //store downloadURL
                        let downloadURL = metaData!.downloadURL()!.absoluteString
                        print(downloadURL)
                        post.image = downloadURL
                        
                        post.isCompleted = true
                        
                    }
                    
                }
                
                
                if picker == imagePicker {
                    data.append(post)
                    tableView.reloadData()
                }else{
                    data1.append(post)
                    videoTableView.reloadData()
                }
                
                
                let observer = uploadTask?.observe(.progress) { snapshot in
                    print(snapshot.progress) // NSProgress object
                    post.progress = (snapshot.progress?.fractionCompleted)!
                    self.tableView.reloadData()
                }
            }
        }else{
            
            let post = ImagePost(title: "", type: ImagePost.TYPE.VIDEO)
            
            if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                // we selected a video
                print("Here's the file URL: ", videoURL)
                
                // Where we'll store the video:
                
                let filePath = "\(String(describing: DataPersistor.sharedInstance.getUserId()!))/\("post")/\(String(describing: DataPersistor.sharedInstance.getUserId()!))\(Date().ticks).MOV"
                let storageReference = self.storageRef?.child(filePath)
                
                post.videoURL = videoURL
                
                
                if picker == imagePicker {
                    data.append(post)
                    tableView.reloadData()
                }else{
                    data1.append(post)
                    videoTableView.reloadData()
                }
                
                let metaData = StorageMetadata()
                
                metaData.contentType = "image/video"
                
                // Start the video storage process
                uploadTask = storageReference?.putFile(from: videoURL as URL, metadata: metaData, completion: { (metadata, error) in
                    if error == nil {
                        print("Successful video upload")
                        let downloadURL = metadata?.downloadURL()!.absoluteString
                        print(downloadURL)
                        post.video = downloadURL
                        
                        post.isCompleted = true
                        
                    } else {
                        print(error?.localizedDescription)
                    }
                })
                
                let observer = uploadTask?.observe(.progress) { snapshot in
                    print(snapshot.progress) // NSProgress object
                    post.progress = (snapshot.progress?.fractionCompleted)!
                    self.tableView.reloadData()
                }
                
            }
        }
        
        
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onVideoTap(_ sender: Any) {
        videoContainer.isHidden = false
    }
    
    @IBAction func onImageTap(_ sender: Any) {
        imageContainer.isHidden = false
    }

    
    @IBAction func onImageUploadTap(_ sender: Any) {
        
        if (isFinishedVideo()) {
            onImageChosserClick(sender: sender as! UITapGestureRecognizer, isVideo: false)
        }else{
            self.view.makeToast("Please wait for video to finish upload")
        }
    }
   
    @IBAction func onAddMemberTap(_ sender: Any) {
    }
    
    @IBAction func onUploadVideoTap(_ sender: Any) {
        if (isFinishedVideo()) {
            onImageChosserClick(sender: sender as! UITapGestureRecognizer, isVideo: true)
        }else{
            self.view.makeToast("Please wait for video to finish upload")
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSubmitPostTap(_ sender: Any) {
        
        if isFinished(){
            
            guard let pText = postText.text, !pText.isEmpty else {
                self.view.makeToast("Please enter title")
                return
            }
            
            guard let descText = descTxt.text, !descText.isEmpty else {
                self.view.makeToast("Please enter description")
                return
            }
            
            
            let p = Item()
            p.title = pText
            p.description = descText
            
            
            
            p.image = ""
            //p.video = ""
            
            var imgStr = ""
            
            var index = 0
            for d in data{
                
                if d.type == ImagePost.TYPE.IMAGE || d.type == ImagePost.TYPE.VIDEO{
                    
                    
                    var str = ""
                    
                    if d.type == ImagePost.TYPE.VIDEO{
                        str = d.video!
                    }else{
                        str = d.image!
                    }
                    
                    if data.count > 1{
                        
                        if index == 0 {
                            imgStr = str
                        }else{
                            imgStr = imgStr + "|" + str
                        }
                    }else{
                        imgStr = str
                    }
                    
                    index = index + 1
                }
                
            }
            // p.postType = "image"
            p.image = imgStr
            
            p.userName = DataPersistor.sharedInstance.getUserName()
            p.userId = String(describing: DataPersistor.sharedInstance.getUserId()!)
            
            
            let params = Mapper().toJSON(p)
            
            let callService = GenericService(view: self.view!, serviceCallback : self)
            callService?.execute(Constants.SELL_ITEMS, bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.sell_items, showLoader: true)
        }else{
            
            self.view.makeToast("Please wait for \(mediaType) to finish upload")
        }
        
    }
    
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.sell_items:
                //moveToLogin()
                print("")
                self.view.makeToast("Item added")
                self.navigationController?.popToRootViewController(animated: true)
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
    
    func onImageChosserClick(sender: UITapGestureRecognizer, isVideo: Bool){
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera(sender: sender, isVideo: isVideo)
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary(sender: sender, isVideo: isVideo)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        //?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: alert)
            popover!.present(from: (sender.view?.frame)!, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
        
    }
    
    func openCamera(sender: UITapGestureRecognizer, isVideo: Bool)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            if isVideo == true{
                videoPicker.allowsEditing = false
                videoPicker.sourceType = .camera
                videoPicker.cameraCaptureMode = .photo
                videoPicker.showsCameraControls = true
                self .present(videoPicker, animated: true, completion: nil)
            }else{
            
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.showsCameraControls = true
            self .present(imagePicker, animated: true, completion: nil)
            }
        }
        else
        {
            openGallary(sender: sender, isVideo: isVideo)
        }
    }
    func openGallary(sender: UITapGestureRecognizer, isVideo: Bool)
    {
        if isVideo == true{
            
            videoPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(videoPicker, animated: true, completion: nil)
            }
            else
            {
                popover = UIPopoverController(contentViewController: videoPicker)
                popover!.present(from: (sender.view?.frame)!, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            }
            
            
        }else{
        
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: imagePicker)
            popover!.present(from: (sender.view?.frame)!, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

