//
//  Event.swift
//  GolfingBudz
//
//  Created by Surya on 29/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: Post {
    
    var description:String?
    var eventsAttendess:[String]?
    var isActive:Bool?
    var title: String?
    var date: String?
    var time: String?
    
    override init() {
        super.init()
    }
    
    // MARK: Mappable
    required init?(map: Map) {
        // subClasses must call the constructor of the base class
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        description <- map["description"]
        eventsAttendess <- map["eventsAttendess"]
        isActive <- map["isActive"]
        title <- map["title"]
        date <- map["date"]
        time <- map["time"]
    }
    
}
