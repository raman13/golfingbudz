//
//  ChatViewController.swift
//  GolfingBudz
//
//  Created by Surya on 26/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import ObjectMapper

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ServiceCallback{

    @IBOutlet weak var chatTxt: UITextField!
    
    @IBOutlet weak var btnSend: UIButton!
     @IBOutlet weak var viewMsg: UIView!
    @IBOutlet weak var tableView: DesignableTableView!
    
    var data = [Comment]()
    
    var postId:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KeyboardAvoiding.avoidingView = viewMsg
        tableView.estimatedRowHeight = 90.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.hideKeyboardWhenTappedAround()
        chatTxt.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationController?.navigationBar.isHidden = false
        
        updateTextField(text: chatTxt, placeHolderText: "Write a message...")

        // Do any additional setup after loading the view.
        getComments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if chatTxt == textField
        {
            chatTxt.text = textField.text
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    @IBAction func onPostChat(_ sender: Any) {
        
        guard let pText = chatTxt.text, !pText.isEmpty else {
            self.view.makeToast("Please enter comment")
            return
        }
        
        postComment()

        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if data.count > 0{
            tableView.backgroundView = .none
        }else{
            Helper.emptyMessage(message: "There are no comments", viewController: tableView)
        }
        
        
        return data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let index = indexPath.item
        
        let item = data[index]
        
        let size = CGSize(width: view.frame.width - 115, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        
        let rectangleHeight = String(item.text!).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        if(rectangleHeight < 40)
        {
            return 90
        }
        else
        {
        return rectangleHeight+50
        }
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        let cellId = "commentCell"
        
        let item = data[index]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CommentViewCell
        
        if let imgUrl = item.userImgUrl {
            if let url = URL.init(string: imgUrl) {
                cell.img.downloadedFrom(url: url)
            }
        }
        
        cell.img.layer.borderWidth = 0
        cell.img.layer.masksToBounds = false
        cell.img.layer.borderColor = UIColor.black.cgColor
        cell.img.layer.cornerRadius = cell.img.frame.height/2
        cell.img.clipsToBounds = true
        
        cell.comment.text = item.text!
        cell.comment.numberOfLines = 0
        cell.usrName.text = item.userName!
        
        return cell
        
    }
    
    func getComments(){
        
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getPostCommentUrl(postId: self.postId!), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_comments, showLoader: true)
    }
    
    func postComment(){
        
        let p = Comment()
        p.userName = DataPersistor.sharedInstance.getUserName()
        p.userId = DataPersistor.sharedInstance.getUserId()!
        p.text = chatTxt.text!
        p.postId = self.postId!
        
        let params = Mapper().toJSON(p)
        let userId = DataPersistor.sharedInstance.getUserId()
        let callService = GenericService(view: self.view!, serviceCallback : self)
        callService?.execute(Constants.getSendPostCommentUrl(), bodyParams: params as [String : AnyObject], method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.post_comment, showLoader: true)
    }
    
    func updateTextField(text: UITextField, placeHolderText: String) {
        text.attributedPlaceholder = NSAttributedString(string: placeHolderText,
                                                        attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 32))
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 28, height: 28));
        
        indentView.backgroundColor = UIColor.clear
        
        imgView.image = UIImage(named: "avatar")
        //  indentView.addSubview(imgView)
       // text.leftView = indentView
        //text.leftViewMode = .always
        
       // text.backgroundColor = UIColor(red: 19/255, green: 19/255, blue: 19/255, alpha: 1)
        text.backgroundColor = UIColor.clear
        text.layer.borderColor = UIColor.clear.cgColor
        
        text.layer.cornerRadius = 8
        text.layer.borderWidth = 1
        // text.layer.borderColor = UIColor.clear as! CGColor
        
        text.useUnderline()
        
    }

    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.get_comments:
                //moveToLogin()
                data.removeAll()
                if let res = Mapper<Response<Comment>>().map(JSONObject: response.response){
                    data = res.data!
                }
                tableView.reloadData()
            
            case ServiceResponse.SERVICE_TYPE.post_comment:
                //moveToLogin()
                getComments()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }

}
