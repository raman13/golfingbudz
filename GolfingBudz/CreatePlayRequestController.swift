//
//  CreatePlayRequestController.swift
//  GolfingBudz
//
//  Created by Surya on 25/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import DLRadioButton
import ObjectMapper
import DLRadioButton
import DropDown
import Scrollable

class CreatePlayRequestController: UIViewController, UITextViewDelegate, ServiceCallback {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var golfClubTxt: UITextField!
    
    @IBOutlet weak var dateTxt: UITextField!
    @IBOutlet weak var timeTxt: UITextField!
    @IBOutlet weak var descTxt: UITextField!
    
    @IBOutlet weak var r18Hole: DLRadioButton!
    
    @IBOutlet weak var r9Hole: DLRadioButton!
    
    @IBOutlet weak var r2: DLRadioButton!
    
    @IBOutlet weak var r3: DLRadioButton!
    
    @IBOutlet weak var r4: DLRadioButton!
    
    @IBOutlet weak var rYouWalking: DLRadioButton!
    
    @IBOutlet weak var rTakingGolf: DLRadioButton!
    
    @IBOutlet weak var txtNumHandicap: UITextField!
    @IBOutlet weak var rSocial: DLRadioButton!
    
    @IBOutlet weak var numHandiView: UIView!
    @IBOutlet weak var rBusiness: DLRadioButton!
    @IBOutlet weak var rYes: DLRadioButton!
    
    @IBOutlet weak var rNo: DLRadioButton!
    
    @IBOutlet weak var pubInvite: DLRadioButton!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewRefined: UIView!
    @IBOutlet weak var txtRegion: UITextField!
    
    @IBOutlet weak var txtIndustry: UITextField!
    
    @IBOutlet weak var rMale: DLRadioButton!
    @IBOutlet weak var txtAffiliate: UITextField!
    
    @IBOutlet weak var heightConst: NSLayoutConstraint!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var rFemale: DLRadioButton!
    @IBOutlet weak var txtSession: UITextField!
    
    let clubs = DropDown()
    var clubData = [String]()
    
    let members = DropDown()
    var membersData = [String]()
    
    let region = DropDown()
    var regionData = [String]()
    
    let industry = DropDown()
    var industryData = [String]()
    
    let profession = DropDown()
    var profData = [String]()
    
    let affliate = DropDown()
    var affData = [String]()
    
    @IBOutlet weak var lblAge: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.hideKeyboardWhenTappedAround()
        
        dateTxt.attributedPlaceholder = NSAttributedString(string: "Day",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        timeTxt.attributedPlaceholder = NSAttributedString(string: "10:10",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        descTxt.attributedPlaceholder = NSAttributedString(string: "Play Request Details",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        txtRegion.attributedPlaceholder = NSAttributedString(string: "Select Region",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        txtIndustry.attributedPlaceholder = NSAttributedString(string: "Select Industry",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        txtSession.attributedPlaceholder = NSAttributedString(string: "Club Member",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        txtAffiliate.attributedPlaceholder = NSAttributedString(string: "No",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        txtNumHandicap.attributedPlaceholder = NSAttributedString(string: "No. of handicap",
                                                                attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        
        dateTxt.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        timeTxt.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        descTxt.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        golfClubTxt.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
         txtRegion.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
         txtSession.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
         txtIndustry.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
         txtAffiliate.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        txtNumHandicap.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        affData = ["Yes", "NO"]

        
        
        clubs.anchorView = golfClubTxt
        clubs.dataSource = clubData
        
        region.anchorView = txtRegion
        region.dataSource = regionData
        
        industry.anchorView = txtIndustry
        industry.dataSource = industryData
        
        profession.anchorView = txtSession
        profession.dataSource = profData
        
        affliate.anchorView = txtAffiliate
        affliate.dataSource = affData
        
        viewHeight.constant = 0
        viewRefined.isHidden = true
        
        heightConst.constant = 0
        numHandiView.isHidden = true
        
        clubs.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.golfClubTxt.text = item
        }
        
        region.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.txtRegion.text = item
        }
        
        industry.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.txtIndustry.text = item
        }
        
        profession.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.txtSession.text = item
        }
        
        affliate.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.txtAffiliate.text = item
        }
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        submitBtn.backgroundColor = .clear
        submitBtn.layer.cornerRadius = 0
        submitBtn.contentEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        Scrollable.createContentView(scrollView)
        
        getCategories()
        getIndustry()
        getRegion()
        getProfession()
         scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: submitBtn.frame.origin.y+10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.tintColor = UIColor.white
    }
    
    func getCategories(){
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
        vehicleService?.execute(Constants.getCategories(), bodyParams: nil, method: .get, token:"", type: ServiceResponse.SERVICE_TYPE.get_categories, showLoader: true)
    }
    
    func getRegion(){
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
        vehicleService?.execute(Constants.getRegion(), bodyParams: nil, method: .get, token:"", type:
            ServiceResponse.SERVICE_TYPE.get_region, showLoader: true)
    }
    
    func getProfession(){
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
        vehicleService?.execute(Constants.getProfession(), bodyParams: nil, method: .get, token:"", type:
            ServiceResponse.SERVICE_TYPE.get_profession, showLoader: true)
    }
    
    func getIndustry(){
        
        let vehicleService = GenericService(view: self.view!, serviceCallback : self)
        vehicleService?.execute(Constants.getIndustry(), bodyParams: nil, method: .get, token:"", type:
            ServiceResponse.SERVICE_TYPE.get_industry, showLoader: true)
    }
    
    @IBAction func onClubTap(_ sender: Any) {
        
        clubs.show()
    }
    @IBAction func onHandicapYesTap(_ sender: Any) {
        if rHandYes != nil{
            heightConst.constant = 30
            numHandiView.isHidden = false
            
        }else{
            heightConst.constant = 0
            numHandiView.isHidden = true
        }
        
    }
    
    @IBOutlet weak var onHandicapNoTap: DLRadioButton!
    
    @IBOutlet weak var rHandYes: DLRadioButton!
    @IBAction func onHandNoTap(_ sender: Any) {
        if onHandicapNoTap != nil{
            heightConst.constant = 0
            numHandiView.isHidden = true
            
        }else{
            heightConst.constant = 30
            numHandiView.isHidden = false
        }
        
   
        
        
    }
    
    @IBAction func onSliderValueChange(_ sender: Any) {
        
        lblAge.text = String(Int(((sender as! UISlider)).value))
        
    }
    @IBAction func on18HoleSelect(_ sender: Any) {
        print((sender as! DLRadioButton).isSelected)
        print(r9Hole.isSelected)
        
    }
    
    @IBAction func onRefineNoTap(_ sender: Any) {
        
        if rNo != nil{
            viewHeight.constant = 0
            viewRefined.isHidden = true
            
        }else{
            viewHeight.constant = 270
            viewRefined.isHidden = false
           
        }
        if(viewRefined.isHidden && numHandiView.isHidden)
        {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 660)
        }
        else if(!viewRefined.isHidden && !numHandiView.isHidden)
        {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 960)
        }
        else if(!viewRefined.isHidden && numHandiView.isHidden)
        {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 930)
        }
    }
    @IBAction func onRefineTap(_ sender: Any) {
        
        if rYes != nil{
            viewHeight.constant = 270
            viewRefined.isHidden = false
            
        }else{
            viewHeight.constant = 0
            viewRefined.isHidden = true
        }
       
        if(viewRefined.isHidden && numHandiView.isHidden)
        {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 660)
        }
        else if(!viewRefined.isHidden && !numHandiView.isHidden)
        {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 960)
        }
        else if(!viewRefined.isHidden && numHandiView.isHidden)
        {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 930)
        }
        
    }
    @IBAction func onSubmitTap(_ sender: Any) {
        
        createPlayRequest()
        
    }
    
    func createPlayRequest(){
              
        if isValid(){
        
        if let p = self.getPlayRequest(){
            
            let params = Mapper().toJSON(p)
            //
            let userId = DataPersistor.sharedInstance.getUserId()
            let callService = GenericService(view: self.view!, serviceCallback : self)
            callService?.execute(Constants.createPlayRequest(), bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.create_play_request, showLoader: true)
        }
        }
    }
    
    func isValid() -> Bool{
        guard let a1 = dateTxt.text, !a1.isEmpty else {
            self.view.makeToast("Please enter date", duration: 3.0, position: .top)
            return false
        }
        
        guard let a2 = golfClubTxt.text, !a2.isEmpty else {
            self.view.makeToast("Please select club", duration: 3.0, position: .top)
            return false
        }
        
        guard let a3 = timeTxt.text, !a3.isEmpty else {
            self.view.makeToast("Please enter time", duration: 3.0, position: .top)
            return false
        }
        
        guard let a4 = descTxt.text, !a4.isEmpty else {
            self.view.makeToast("Please enter request details", duration: 3.0, position: .top)
            return false
        }
        
        return true
    }
    
    
    func getPlayRequest() -> PlayRequest?{
        
       
        
        let req = PlayRequest()
        
        var noHole = "9"
        if r18Hole != nil{
            noHole = "18"
        }
        req.noOfHoles = noHole
        req.day = dateTxt.text
        req.golfClub = golfClubTxt.text
        req.teeOffTime = timeTxt.text
        
        var type = "Bussiness"
            
        if rSocial != nil{
            type = "Social Round"
        }
        
        req.type = type
        
        var isHand = false
        
        if rHandYes != nil {
            isHand = true
        }
        
        req.requestInfo = descTxt.text
        req.handicap = String(isHand)
        req.NoOfHandicaps = txtNumHandicap.text as AnyObject
        req.affiliated = txtAffiliate.text as AnyObject
        req.profession = txtSession.text
        req.industry = txtIndustry.text
        
        
        var gender = "Female"
        
        if rMale != nil{
            gender = "Male"
        }
        req.gender = gender
        req.age = lblAge.text!
        req.locations = txtRegion.text

        
        return req
    }
    
    @IBAction func onRegionTap(_ sender: Any) {
        
        region.show()
        
    }
    
    
    @IBAction func onDateTap(_ sender: Any) {
        
        self.view.endEditing(true)
        
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: UIDatePickerMode.date) {
            (date) -> Void in
            
            let dateFormatter = DateFormatter()
            //  dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "dd/MM/yyyy"
            // value.text = dateFormatter.stringFromDate(sender.date)
            self.dateTxt.text = dateFormatter.string(from: date)
        }
        
    }
    
    @IBAction func onTimeTap(_ sender: Any) {
        
        self.view.endEditing(true)
        DatePickerDialog().show("Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: UIDatePickerMode.time) {
            (date) -> Void in
            
            let dateFormatter = DateFormatter()
            //  dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "HH:mm"
            // value.text = dateFormatter.stringFromDate(sender.date)
            self.timeTxt.text = dateFormatter.string(from: date)
        }
        
    }

    
    
    @IBAction func onAffiliateTap(_ sender: Any) {
        affliate.show()
    }
    
    @IBAction func onProfessionTap(_ sender: Any) {
        
        profession.show()
    }
    @IBAction func onIndustryTap(_ sender: Any) {
        
        industry.show()
        
    }
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.create_play_request:
                
                self.navigationController?.popToRootViewController(animated: false)
                
            case ServiceResponse.SERVICE_TYPE.get_region:
                
                self.regionData.removeAll()
                
                if let countryResponse = Mapper<Response<Category>>().map(JSONObject: response.response){
                    var index = 0
                    for d in countryResponse.data!{
                        regionData += [d.displayName!]
                        
                        if index == 0{
                            self.txtRegion.text = d.displayName!
                            index = 1
                        }
                    }
                }
                region.dataSource = regionData
                region.reloadAllComponents()
                
            case ServiceResponse.SERVICE_TYPE.get_industry:
                
                self.industryData.removeAll()
                
                if let countryResponse = Mapper<Response<Category>>().map(JSONObject: response.response){
                    var index = 0
                    for d in countryResponse.data!{
                        industryData += [d.displayName!]
                        
                        if index == 0{
                            self.txtIndustry.text = d.displayName!
                            index = 1
                        }
                    }
                }
                industry.dataSource = industryData
                industry.reloadAllComponents()

                
            case ServiceResponse.SERVICE_TYPE.get_profession:
                
                self.profData.removeAll()
                
                if let countryResponse = Mapper<Response<Category>>().map(JSONObject: response.response){
                    var index = 0
                    for d in countryResponse.data!{
                        profData += [d.displayName!]
                        
                        if index == 0{
                            self.txtSession.text = d.displayName!
                            index = 1
                        }
                    }
                }
                profession.dataSource = profData
                profession.reloadAllComponents()
                
            case ServiceResponse.SERVICE_TYPE.get_categories:
                
                print("Received")
                self.clubData.removeAll()
                
                if let countryResponse = Mapper<Response<Category>>().map(JSONObject: response.response){
                    var index = 0
                    for d in countryResponse.data!{
                        clubData += [d.displayName!]
                        
                        if index == 0{
                            self.golfClubTxt.text = d.displayName!
                            index = 1
                        }
                    }
                }
                clubs.dataSource = clubData
                clubs.reloadAllComponents()
                // data.removeAll()
                // data = (tripResponse?.productWrappers)!
                // self.tableView.reloadData()
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .top)
    }

    

}
