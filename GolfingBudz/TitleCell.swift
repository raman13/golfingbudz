//
//  TitleCell.swift
//  GolfingBudz
//
//  Created by Surya on 19/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
