//
//  PostMediaController.swift
//  GolfingBudz
//
//  Created by Surya on 18/07/2017.
//  Copyright © 2017 Surya. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import ObjectMapper
import MobileCoreServices
import MediaPlayer
import AVKit
import AVFoundation

class CreateEventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, ServiceCallback{
    
    @IBOutlet weak var postText: UITextField!
    
    @IBOutlet weak var date: UITextField!
    
    @IBOutlet weak var PriceTxt: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var descTxt: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    var data = [ImagePost]()
    
    var isImage: Bool = true
    
    let imagePicker = UIImagePickerController()
    
    var popover:UIPopoverController?=nil
    
    var storageRef:StorageReference?
    
    var currentIndex: Int = 0
    
    var uploadTask: StorageUploadTask?
    
    var mediaType: String = "Image"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        let storage = Storage.storage()
        storageRef = storage.reference()
        
        
        let userIcon = UIImageView(image: UIImage(named: "golf"))
        
        //postText.leftView = userIcon
        //postText.leftViewMode = .always
        
        postText.delegate = self
        descTxt.delegate = self
        PriceTxt.delegate = self
        date.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        
        imagePicker.delegate = self
        
        
        submitBtn.backgroundColor = .clear
        submitBtn.layer.cornerRadius = 0
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor(red: 239/255, green: 222/255, blue: 179/255, alpha: 1).cgColor
        
        
        if isImage == false{
            mediaType = "Video"
            imagePicker.mediaTypes =  [kUTTypeMovie as String]
        }
        
       // let t = "Say Something about this \(mediaType)"
        
        postText.attributedPlaceholder = NSAttributedString(string: "Title",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        descTxt.attributedPlaceholder = NSAttributedString(string: "Description",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        PriceTxt.attributedPlaceholder = NSAttributedString(string: "Date",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        date.attributedPlaceholder = NSAttributedString(string: "Time",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        // Do any additional setup after loading the view.
        
        data += [ImagePost(title: "Upload \(mediaType)s", type: ImagePost.TYPE.TITLE)]
        data += [ImagePost(title: "",type: ImagePost.TYPE.UPLOAD)]
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        //  self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        // self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 56/255, green: 56/255, blue: 53/255, alpha: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSubmitTap(_ sender: Any) {
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if postText == textField
        {
            postText.text = textField.text
        }else if descTxt == textField
        {
            descTxt.text = textField.text
        }else if PriceTxt == textField
        {
            PriceTxt.text = textField.text
        }else if date == textField
        {
            date.text = textField.text
        }
        
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if data.count > 0{
            tableView.backgroundView = .none
        }else{
            Helper.emptyMessage(message: "Please Upload an \(mediaType)", viewController: tableView)
        }
        
        
        return data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.item
        
        if data[index].type == ImagePost.TYPE.TITLE{
            return 30
        }else if data[index].type == ImagePost.TYPE.IMAGE || data[index].type == ImagePost.TYPE.VIDEO {
            return 200
        }else{
            return 140
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.item
        
        var cellId = "MediaUploadCell"
        
        let item = data[index]
        
        if item.type == ImagePost.TYPE.IMAGE || item.type == ImagePost.TYPE.VIDEO {
            
            cellId = "PostImageCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ImageViewCell
            
            //cell.backgroundColor = UIColor.clear
            if item.type == ImagePost.TYPE.IMAGE{
                cell.mainImage.image = item.imageUI
            }else{
                
                if cell.mainImage.layer.sublayers == nil{
                    
                    let player = AVPlayer(url: item.videoURL! as URL)
                    
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = cell.mainImage.bounds
                    playerLayer.bounds = cell.mainImage.bounds
                    
                    cell.mainImage.layer.addSublayer(playerLayer)
                    
                    
                    player.play()
                }
                
            }
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(PostMediaController.closeClicked(_:)))
            cell.closeBtn.tag = (indexPath as NSIndexPath).row
            cell.closeBtn.isUserInteractionEnabled = true
            cell.closeBtn.addGestureRecognizer(tapGestureRecognizer)
            
            DispatchQueue.main.async {
                cell.progressBar.setProgress(Float(item.progress), animated: true)
                //                if (!item.isCompleted && item.type == ImagePost.TYPE.MEDIA){
                //                    tableView.reloadData()
                //                }
                // tableView.reloadInputViews()
                // cell.progressBar.reloadInputViews()
            }
            
            return cell
        }else if item.type == ImagePost.TYPE.TITLE{
            
            cellId = "TitleCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TitleCell
            
            //cell.backgroundColor = UIColor.clear
            //cell.textLabel?.text = item.title
            
            return cell
        }else{
            cellId = "MediaUploadCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MediaUploadCell
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(PostMediaController.uploadClicked(_:)))
            cell.uploadBtn.tag = (indexPath as NSIndexPath).row
            cell.uploadBtn.isUserInteractionEnabled = true
            cell.uploadBtn.addGestureRecognizer(tapGestureRecognizer)
            
            return cell
        }
        
    }
    
    func playVideo(_ sender: UITapGestureRecognizer){
        
        let index = (sender.view?.tag)!
        
        
    }
    
    func closeClicked(_ sender: UITapGestureRecognizer){
        
        let index = (sender.view?.tag)!
        //let d = data[index]
        
        uploadTask?.cancel()
        
        data.remove(at: index)
        tableView.reloadData()
        tableView.reloadInputViews()
        
    }
    
    func uploadClicked(_ sender: UITapGestureRecognizer){
        
        let index = (sender.view?.tag)!
        //let d = data[index]
        
        currentIndex = index
        
        if (isFinished()) {
            onImageChosserClick(sender: sender, index: index)
        }else{
            self.view.makeToast("Please wait for \(mediaType.localizedLowercase) to finish upload")
        }
    }
    
    func isFinished() -> Bool{
        for d in data{
            if !d.isCompleted && (d.type == ImagePost.TYPE.IMAGE || d.type == ImagePost.TYPE.VIDEO) {
                return false;
            }
        }
        return true;
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info.debugDescription)
        
        if isImage{
            
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                let post = ImagePost(title: "", type: ImagePost.TYPE.IMAGE)
                
                post.imageUI = pickedImage
                
                let imgData = UIImageJPEGRepresentation(pickedImage, 0.5)!
                // set upload path
                let filePath = "\(String(describing: DataPersistor.sharedInstance.getUserId()!))/\("post")/\(String(describing: DataPersistor.sharedInstance.getUserId()!))\(Date().ticks)img.jpg"
                let metaData = StorageMetadata()
                
                metaData.contentType = "image/jpg"
                uploadTask = self.storageRef?.child(filePath).putData(imgData, metadata: metaData){(metaData,error) in
                    
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }else{
                        //store downloadURL
                        let downloadURL = metaData!.downloadURL()!.absoluteString
                        print(downloadURL)
                        post.image = downloadURL
                        
                        post.isCompleted = true
                        
                    }
                    
                }
                
                data.insert(post, at: (data.count-1))
                tableView.reloadData()
                
                let observer = uploadTask?.observe(.progress) { snapshot in
                    print(snapshot.progress) // NSProgress object
                    post.progress = (snapshot.progress?.fractionCompleted)!
                    self.tableView.reloadData()
                }
            }
        }else{
            
            let post = ImagePost(title: "", type: ImagePost.TYPE.VIDEO)
            
            if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                // we selected a video
                print("Here's the file URL: ", videoURL)
                
                // Where we'll store the video:
                
                let filePath = "\(String(describing: DataPersistor.sharedInstance.getUserId()!))/\("post")/\(String(describing: DataPersistor.sharedInstance.getUserId()!))\(Date().ticks).MOV"
                let storageReference = self.storageRef?.child(filePath)
                
                post.videoURL = videoURL
                
                
                data.insert(post, at: (data.count-1))
                tableView.reloadData()
                
                let metaData = StorageMetadata()
                
                metaData.contentType = "image/video"
                
                // Start the video storage process
                uploadTask = storageReference?.putFile(from: videoURL as URL, metadata: metaData, completion: { (metadata, error) in
                    if error == nil {
                        print("Successful video upload")
                        let downloadURL = metadata?.downloadURL()!.absoluteString
                        print(downloadURL)
                        post.video = downloadURL
                        
                        post.isCompleted = true
                        
                    } else {
                        print(error?.localizedDescription)
                    }
                })
                
                let observer = uploadTask?.observe(.progress) { snapshot in
                    print(snapshot.progress) // NSProgress object
                    post.progress = (snapshot.progress?.fractionCompleted)!
                    self.tableView.reloadData()
                }
                
            }
        }
        
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSubmitPostTap(_ sender: Any) {
        
        if isFinished(){
            
            guard let pText = postText.text, !pText.isEmpty else {
                self.view.makeToast("Please enter title")
                return
            }
            
            guard let descText = descTxt.text, !descText.isEmpty else {
                self.view.makeToast("Please enter description")
                return
            }
            
            guard let priceText = PriceTxt.text, !priceText.isEmpty else {
                self.view.makeToast("Please enter date")
                return
            }
            
            guard let timeText = date.text, !timeText.isEmpty else {
                self.view.makeToast("Please enter time")
                return
            }
            
            let p = Event()
            p.title = pText
            p.description = descText
            p.date = priceText
            p.time = timeText
            
            
            p.image = ""
            //p.video = ""
            
            var imgStr = ""
            
            var index = 0
            for d in data{
                
                if d.type == ImagePost.TYPE.IMAGE || d.type == ImagePost.TYPE.VIDEO{
                    
                    
                    var str = ""
                    
                    if d.type == ImagePost.TYPE.VIDEO{
                        str = d.video!
                    }else{
                        str = d.image!
                    }
                    
                    if data.count > 1{
                        
                        if index == 0 {
                            imgStr = str
                        }else{
                            imgStr = imgStr + "|" + str
                        }
                    }else{
                        imgStr = str
                    }
                    
                    index = index + 1
                }
                
            }
            // p.postType = "image"
            p.image = imgStr
            
            p.userName = DataPersistor.sharedInstance.getUserName()
            p.userId = String(describing: DataPersistor.sharedInstance.getUserId()!)
            
            
            let params = Mapper().toJSON(p)
            
            let callService = GenericService(view: self.view!, serviceCallback : self)
            callService?.execute(Constants.GET_EVENTS, bodyParams: params as [String : AnyObject]?, method: .post, token:"", type: ServiceResponse.SERVICE_TYPE.post_event, showLoader: true)
        }else{
            
            self.view.makeToast("Please wait for \(mediaType) to finish upload")
        }
        
    }
    
    
    func onSuccess(_ response: ServiceResponse)
    {
        if response.status == 200{
            
            switch response.getType(){
                
            case ServiceResponse.SERVICE_TYPE.post_event:
                //moveToLogin()
                print("")
                self.view.makeToast("Event created")
                self.navigationController?.popToRootViewController(animated: true)
                
            default:
                print("")
            }
            // print(response)
        }
    }
    
    func onError(_ error: String, type: ServiceResponse.SERVICE_TYPE)
    {
        // toast with a specific duration and position
        self.view.makeToast(error, duration: 3.0, position: .center)
    }
    
    
    @IBAction func onDateTap(_ sender: Any) {
        
        self.view.endEditing(true)
        
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: UIDatePickerMode.date) {
            (date) -> Void in
            
            let dateFormatter = DateFormatter()
            //  dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "dd/MM/yyyy"
            // value.text = dateFormatter.stringFromDate(sender.date)
            self.PriceTxt.text = dateFormatter.string(from: date)
        }
        
    }
    
    @IBAction func onTimeTap(_ sender: Any) {
        
        self.view.endEditing(true)
        DatePickerDialog().show("Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: UIDatePickerMode.time) {
            (date) -> Void in
            
            let dateFormatter = DateFormatter()
            //  dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "HH:mm"
            // value.text = dateFormatter.stringFromDate(sender.date)
            self.date.text = dateFormatter.string(from: date)
        }

    }
    
    func onImageChosserClick(sender: UITapGestureRecognizer, index: Int){
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera(index: index, sender: sender)
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary(index: index, sender: sender)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        //?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: alert)
            popover!.present(from: (sender.view?.frame)!, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
        
    }
    
    func openCamera(index: Int, sender: UITapGestureRecognizer)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.showsCameraControls = true
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            openGallary(index: index, sender: sender)
        }
    }
    func openGallary(index: Int, sender: UITapGestureRecognizer)
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: imagePicker)
            popover!.present(from: (sender.view?.frame)!, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

